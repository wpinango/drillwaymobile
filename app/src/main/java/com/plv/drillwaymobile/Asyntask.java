package com.plv.drillwaymobile;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.plv.drillwaymobile.dwmodels.Constant;
import com.plv.drillwaymobile.interfaces.OnRTDataRequested;
import com.plv.drillwaymobile.interfaces.OnTaskCompleted;
import com.plv.drillwaymobile.models.Response;

/**
 * Created by wpinango on 2/27/18.
 */

public class Asyntask {
    private static int timeout = 10000;
    //private static String url = "http://192.168.0.10:8080";
    private static String KEY_HASH = "key_hash";
    public static String testHeader = "12345";
    private static String port = "8080";

    public static class GetMethodAsynctask extends AsyncTask<String, String, String>  {
        private Context context;
        private HttpRequest request;
        private String endPoint;
        private String header;
        private String url;
        private OnTaskCompleted onTaskCompleted;
        private Response response;

        public GetMethodAsynctask(Context context, String endPoint, String header, OnTaskCompleted onTaskCompleted, String address) {
            this.endPoint = endPoint;
            this.context = context;
            this.header = header;
            this.onTaskCompleted = onTaskCompleted;
            this.url = "http://" + address +  ":" +  port + "/";
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.get(url + endPoint)
                        .accept("application/json")
                        .header(KEY_HASH, header)
                        .connectTimeout(timeout)
                        .readTimeout(timeout);
                response = new Gson().fromJson(request.body(), Response.class);
                return response.getMessage();
            } catch (HttpRequest.HttpRequestException ex) {
                Global.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                onTaskCompleted.onTaskCompleted(OnTaskCompleted.RTOffLine, ex.getMessage());
                return "";
            }
        }

        @Override
        protected void onPostExecute(String o) {
            if (!o.equals("") && response.getStatus() == 200) {
                if (endPoint.equals(Constant.EP_GET_RTDATA)) {
                    onTaskCompleted.onTaskCompleted(OnTaskCompleted.RTDATA, o);
                }
                else if (endPoint.equals(Constant.EP_GET_RIG)) {
                    onTaskCompleted.onTaskCompleted(OnTaskCompleted.RIGDATA,o);
                }
            }
        }
    }

    public static class PutMethodAsyntask extends AsyncTask<String, String, String> {

        private Context context;
        private HttpRequest request;
        private String url;

        public PutMethodAsyntask(Context context, String url) {
            this.url = url;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.put(url)
                        .accept("application/json")
                        .connectTimeout(timeout)
                        .readTimeout(timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                Global.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);

        }
    }

    public static class DeleteMethodAsyntask extends AsyncTask<String,String,String>{

        private Context context;
        private HttpRequest request;
        private String url;

        public DeleteMethodAsyntask(Context context, String url) {
            this.url = url;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.delete(url)
                        .accept("application/json")
                        .connectTimeout(timeout)
                        .readTimeout(timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                Global.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);

        }
    }

    public static class PostMethodAyntask extends AsyncTask<String,String,String> {

        private Context context;
        private HttpRequest request;
        private String url;

        public PostMethodAyntask(Context context, String url) {
            this.url = url;
            this.context = context;
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                request = HttpRequest.post(url)
                        .accept("application/json")
                        .connectTimeout(timeout)
                        .readTimeout(timeout);
                return request.body();
            } catch (HttpRequest.HttpRequestException ex) {
                Global.Toaster.get().showToast(context, ex.getMessage(), Toast.LENGTH_SHORT);
                return "";
            }
        }

        @Override
        protected void onPostExecute(String o) {
            super.onPostExecute(o);

        }
    }
}
