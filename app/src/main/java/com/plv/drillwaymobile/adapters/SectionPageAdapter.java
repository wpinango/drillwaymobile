package com.plv.drillwaymobile.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.plv.drillwaymobile.fragments.ChartFragment;
import com.plv.drillwaymobile.fragments.NumericFragment;
import com.plv.drillwaymobile.models.BoxModel;

import java.util.ArrayList;

/**
 * Created by wpinango on 2/7/18.
 */

public class SectionPageAdapter extends FragmentPagerAdapter {
    private NumericFragment numericFragment = new NumericFragment();
    private ChartFragment chartFragment = new ChartFragment();


    public SectionPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return numericFragment;
            case 1:
                return chartFragment;
        }

        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Numeric";
            case 1:
                return "Chart";
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void setValues(ArrayList<BoxModel> values) {
        numericFragment.setValues(values);
    }

    public void updateValue(){
        numericFragment.updateValues();
    }
}
