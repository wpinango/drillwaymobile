package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.rigdata.DaqSetting;
import com.plv.drillwaymobile.interfaces.OnEditListener;

import java.util.ArrayList;

/**
 * Created by wpinango on 3/14/18.
 */

public class SourceListAdapter1 extends BaseAdapter {
    private Context context;
    private ArrayList<DaqSetting>sources;
    private LayoutInflater layoutInflater;
    private OnEditListener listener;

    public SourceListAdapter1(Context context, ArrayList<DaqSetting> sources, OnEditListener listener) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.sources = sources;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return sources.size();
    }

    @Override
    public Object getItem(int i) {
        return sources.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_source, null);
        }
        DaqSetting source = sources.get(i);
        //System.out.println("valores : " + source.getIp() + " " + source.getPort());
        TextView etIp = view.findViewById(R.id.tv_ip_source);
        TextView etPort = view.findViewById(R.id.tv_port_source);
        TextView tvDevice = view.findViewById(R.id.tv_device_source);
        ImageButton btnEdit = view.findViewById(R.id.btn_edit_source);
        Switch swStatus = view.findViewById(R.id.sw_state_source);
        try {
            etIp.setText(source.getHost());
            etPort.setText(String.valueOf(source.getPort()));
            tvDevice.setText(source.getSource().name());
            //swStatus.setChecked(source.getStatus().name());
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClickEditSource(source, i);
                }
            });
            swStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    listener.onChangeStatusSource(b, i);
                }
            });
        } catch (Exception e ){
            e.getMessage();
        }
        return view;
    }
}
