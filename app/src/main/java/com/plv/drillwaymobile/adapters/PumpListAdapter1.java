package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.dwmodels.rigdata.Pump;
import com.plv.drillwaymobile.interfaces.OnEditListener;

import java.util.ArrayList;

/**
 * Created by wpinango on 3/14/18.
 */

public class PumpListAdapter1 extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Pump> pumps;
    private OnEditListener listener;

    public PumpListAdapter1(Context context, ArrayList<Pump>pumps, OnEditListener listener) {
        this.context = context;
        this.pumps = pumps;
        this.layoutInflater = LayoutInflater.from(context);
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return pumps.size();
    }

    @Override
    public Object getItem(int i) {
        return pumps.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_pump,null);
        }
        TextView tvPumpNumber = view.findViewById(R.id.tv_pump_number);
        TextView tvLength = view.findViewById(R.id.tv_item_pump_length);
        TextView tvDiameter = view.findViewById(R.id.tv_item_pump_diameter);
        TextView tvEfficient = view.findViewById(R.id.tv_item_pump_efficient);
        ImageButton imgEdit = view.findViewById(R.id.img_item_pump_edit);
        Pump pump = pumps.get(i);
        try {
            tvEfficient.setText(String.valueOf(pump.getEfficient()));
            tvDiameter.setText(String.valueOf(pump.getDiameter()));
            tvLength.setText(String.valueOf(pump.getLength()));
            tvPumpNumber.setText(String.valueOf(i + 1));
            imgEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClickEditPump(pump, i);
                }
            });
        }catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
