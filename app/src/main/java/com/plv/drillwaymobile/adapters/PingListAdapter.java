package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.plv.drillwaymobile.R;

import java.util.ArrayList;

/**
 * Created by wpinango on 2/27/18.
 */

public class PingListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<String> pingResults;
    private LayoutInflater layoutInflater;

    public PingListAdapter(Context context,ArrayList<String> pingResults) {
        this.context = context;
        this.pingResults = pingResults;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return pingResults.size();
    }

    @Override
    public Object getItem(int i) {
        return pingResults.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_ping, null);
        }
        TextView tvPing = (TextView)view.findViewById(R.id.tv_ping_result);
        try {
            tvPing.setText(pingResults.get(i));
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
