package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.plv.drillwaymobile.Global;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.models.BoxModel;
import com.plv.drillwaymobile.utils.AppTheme;
import com.plv.drillwaymobile.utils.Format;
import com.plv.drillwaymobile.widgets.DWTextView;

import java.util.ArrayList;

/**
 * Created by wpinango on 2/14/18.
 */

public class NumericGridAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<BoxModel> values;


    public NumericGridAdapter(Context context, ArrayList<BoxModel> values) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.values = values;
    }

    @Override
    public int getCount() {
        return values.size();
    }

    @Override
    public Object getItem(int i) {
        return values.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_grid_numeric, null);
        }
        DWTextView tvValue = view.findViewById(R.id.tv_value);
        DWTextView tvValueName = view.findViewById(R.id.tv_value_name);
        TextView tvValueUnit = view.findViewById(R.id.tv_value_unit);
        CardView cardView = view.findViewById(R.id.cv_numeric);
        AppTheme appTheme = new AppTheme(context);
        /*if (appTheme.getAppTheme() == R.style.LightAppTheme_NoActionBar){
            //cardView.setBackgroundResource(R.color.white);
            cardView.setBackgroundColor(Color.WHITE);
        } else {
            //cardView.setBackgroundResource(R.color.blueDark);
            cardView.setBackgroundColor(Color.BLACK);
        }*/
        try {
            BoxModel value = values.get(i);
            //System.out.println("valores " + Global.rtData.getVariableValue(Enum.MNemonic.valueOf(value.getNemonic().name())));
            tvValue.setText(Format.getFormattedStringTwoDecimal(Global.rtData.getVariableValue(Enum.MNemonic.valueOf(value.getNemonic().name()))));
            tvValueName.setText(Global.user.getVarDataLocal(Enum.MNemonic.valueOf(value.getNemonic().name())).getDisplay());
            tvValueUnit.setText(Global.rtData.getDataMaster(Enum.MNemonic.valueOf(value.getNemonic().name())).getBaseUnit().name);
            //System.out.println("valores 2 : " + Global.rtData.getDataMaster(Enum.MNemonic.valueOf(value.getNemonic().name())).getBaseUnit().name);
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }
}
