package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.Switch;
import android.widget.TextView;

import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.interfaces.OnEditListener;
import com.plv.drillwaymobile.models.Source;

import io.realm.OrderedRealmCollection;
import io.realm.RealmBaseAdapter;

/**
 * Created by wpinango on 3/7/18.
 */

public class SourceListAdapter extends RealmBaseAdapter<Source> implements ListAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private OrderedRealmCollection<Source> sources;
    private OnEditListener listener;


    public SourceListAdapter(Context context, OrderedRealmCollection<Source> data, OnEditListener listener) {
        super(data);
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.sources = data;
        this.listener = listener;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_source, null);
        }
        Source source = sources.get(i);
        //System.out.println("valores : " + source.getIp() + " " + source.getPort());
        TextView etIp = view.findViewById(R.id.tv_ip_source);
        TextView etPort = view.findViewById(R.id.tv_port_source);
        TextView tvDevice = view.findViewById(R.id.tv_device_source);
        ImageButton btnEdit = view.findViewById(R.id.btn_edit_source);
        Switch swStatus = view.findViewById(R.id.sw_state_source);
        try {
            etIp.setText(source.getIp());
            etPort.setText(source.getPort());
            tvDevice.setText(source.getDevice());
            swStatus.setChecked(source.isStatus());
            btnEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //listener.onClickEditSource(source, i);
                }
            });
            swStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    listener.onChangeStatusSource(b, i);
                }
            });
        } catch (Exception e ){
            e.getMessage();
        }
        return view;
    }
}
