package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.plv.drillwaymobile.R;

import java.util.List;

/**
 * Created by wpinango on 2/15/18.
 */

public class WifiListAdapter extends BaseAdapter {
    private Context context;
    private List<ScanResult> scanResults;
    private LayoutInflater inflater;
    private WifiInfo wifiInfo;

    public WifiListAdapter(Context context, List<ScanResult>scanResults, WifiInfo wifiInfo) {
        this.context = context;
        this.scanResults = scanResults;
        this.inflater = LayoutInflater.from(context);
        this.wifiInfo = wifiInfo;
    }

    @Override
    public int getCount() {
        return scanResults.size();
    }

    @Override
    public Object getItem(int i) {
        return scanResults.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = inflater.inflate(R.layout.item_list_wifi, null);
        }
        ScanResult scanResult = scanResults.get(i);
        TextView tvSsid = view.findViewById(R.id.tv_wifi_ssid);
        TextView tvSecurity = view.findViewById(R.id.tv_wifi_security);
        TextView tvConnectionStatus = view.findViewById(R.id.tv_connection_status);
        ImageView imgWifi = view.findViewById(R.id.img_wifi);
        try {
            tvSsid.setText(scanResult.SSID);
            tvSecurity.setText(scanResult.capabilities);
            if (wifiInfo.getSSID().replaceAll("\"","").equals(scanResult.SSID)){
                tvConnectionStatus.setText(R.string.connected);
            }
            else if (wifiInfo.getBSSID() == null && wifiInfo.getBSSID() != scanResult.BSSID) {
                tvConnectionStatus.setText("");
            } else {
                tvConnectionStatus.setText("");
            }
            if (scanResult.capabilities.contains("WPA")){
                imgWifi.setImageResource(R.drawable.wifi_wp);
            } else {
                imgWifi.setImageResource(R.drawable.wifi);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }

    public void updateWifiInfo(WifiInfo wifiInfo){
        this.wifiInfo = wifiInfo;
    }
}
