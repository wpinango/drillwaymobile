package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.rigdata.DaqSetting;
import com.plv.drillwaymobile.dwmodels.rigdata.RigData;

import java.util.Map;

/**
 * Created by wpinango on 4/12/18.
 */

public class DeviceListAdapter1 extends BaseExpandableListAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private RigData rigData;

    public DeviceListAdapter1(Context context, RigData rigData) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        this.rigData = rigData;
    }

    @Override
    public int getGroupCount() {
        return rigData.getDaqSettings().size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 0;
    }

    @Override
    public Object getGroup(int i) {
        int k = 0;
        for (Map.Entry<Enum.Source,DaqSetting> entry : rigData.getDaqSettings().entrySet()) {
            if (k++ == i) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        //rigData.getVariablesDataMaster();
        /*int k = 0;
        for (Map.Entry<Enum.Source,DaqSetting> entry : rigData.getDaqSettings().entrySet()) {
            if (k++ == i) {
                return entry.getValue();
            }

        }*/
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_devices_type, null);
        }
        TextView tvDeviceName = view.findViewById(R.id.tv_item_device_name);
        TextView tvDeviceIp = view.findViewById(R.id.tv_item_device_ip);
        TextView tvDevicePort = view.findViewById(R.id.tv_item_device_port);
        try {
            DaqSetting source = (DaqSetting) getGroup(i);
            tvDeviceName.setText(source.getSource().name());
            tvDeviceIp.setText(source.getHost());
            tvDevicePort.setText(String.valueOf(source.getPort()));
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        return null;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
