package com.plv.drillwaymobile.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.dwmodels.rigdata.DaqSetting;
import com.plv.drillwaymobile.interfaces.OnTaskCompleted;
import com.plv.drillwaymobile.models.Source;

import java.util.ArrayList;

/**
 * Created by wpinango on 3/27/18.
 */

public class DeviceListAdapter extends BaseExpandableListAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<DaqSetting> sources;


    public DeviceListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        //this.sources = sources;
    }

    @Override
    public int getGroupCount() {
        return sources.size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 0;
    }

    @Override
    public Object getGroup(int i) {
        return null;
    }

    @Override
    public Object getChild(int i, int i1) {
        return null;
    }

    @Override
    public long getGroupId(int i) {
        return 0;
    }

    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_devices_type, null);
        }
        TextView tvDeviceName = view.findViewById(R.id.tv_item_device_name);
        TextView tvDeviceIp = view.findViewById(R.id.tv_item_device_ip);
        TextView tvDevicePort = view.findViewById(R.id.tv_item_device_port);
        try {
            DaqSetting source = sources.get(i);
            tvDeviceName.setText(source.getSource().name());
            tvDeviceIp.setText(source.getHost());
            tvDevicePort.setText(String.valueOf(source.getPort()));
        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_channel, null);
        }
        TextView tvChannelNumber = view.findViewById(R.id.tv_item_channel_number);
        TextView tvChannelName = view.findViewById(R.id.tv_item_channel_name);
        TextView tvChannelValue = view.findViewById(R.id.tv_item_channel_value);
        TextView tvChannelVoltage = view.findViewById(R.id.tv_item_channel_voltage);
        try {

        } catch (Exception e) {
            e.getMessage();
        }
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
