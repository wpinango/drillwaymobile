package com.plv.drillwaymobile.interfaces;

import com.plv.drillwaymobile.dwmodels.rigdata.DaqSetting;
import com.plv.drillwaymobile.dwmodels.rigdata.Pump;


/**
 * Created by wpinango on 3/9/18.
 */

public interface OnEditListener {

    void onClickEditSource(DaqSetting source, int position);
    void onChangeStatusSource(boolean status, int position);
    void onClickEditPump(Pump pump, int position);
}
