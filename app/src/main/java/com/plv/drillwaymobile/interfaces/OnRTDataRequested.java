package com.plv.drillwaymobile.interfaces;

/**
 * Created by wpinango on 2/27/18.
 */

public interface OnRTDataRequested {
    void onDataRequested();
}
