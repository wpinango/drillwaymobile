package com.plv.drillwaymobile.interfaces;

/**
 * Created by wpinango on 2/27/18.
 */

public interface OnTaskCompleted {
    String RTDATA = "rtData";
    String RTOffLine = "offline";
    String RIGDATA = "rigData";

    void onTaskCompleted(String type, String task);
}
