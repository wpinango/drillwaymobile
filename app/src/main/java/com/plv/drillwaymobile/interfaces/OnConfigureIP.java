package com.plv.drillwaymobile.interfaces;

/**
 * Created by wpinango on 2/21/18.
 */

public interface OnConfigureIP {

    void onConfigureIPFinish(String SSID, String IP);
}
