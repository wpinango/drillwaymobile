package com.plv.drillwaymobile.interfaces;

import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;

import java.util.List;

/**
 * Created by wpinango on 2/19/18.
 */

public interface OnScanFinished {

    void onResult(List<ScanResult> list, WifiInfo wifiInfo);
}
