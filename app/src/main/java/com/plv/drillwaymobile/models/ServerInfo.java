package com.plv.drillwaymobile.models;

import com.google.gson.Gson;
import com.plv.drillwaymobile.utils.SharedPreferences;

/**
 * Created by wpinango on 3/2/18.
 */

public class ServerInfo {
    private String address;
    private boolean connect;


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isConnect() {
        return connect;
    }

    public void setConnect(boolean connect) {
        this.connect = connect;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

}
