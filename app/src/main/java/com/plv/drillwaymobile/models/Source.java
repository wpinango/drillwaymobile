package com.plv.drillwaymobile.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by wpinango on 3/7/18.
 */

public class Source extends RealmObject {
    @PrimaryKey
    private String id;
    @Required
    private String ip;
    @Required
    private String port;
    @Required
    private String device;
    private boolean status;

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getIp(){
        return ip;
    }

    public String getPort(){
        return port;
    }

    public boolean isStatus(){
        return status;
    }
}
