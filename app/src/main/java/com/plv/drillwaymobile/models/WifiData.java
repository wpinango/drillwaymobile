package com.plv.drillwaymobile.models;

/**
 * Created by wpinango on 2/26/18.
 */

public class WifiData {
    private String SSID;
    private String Ip;

    public String getSSID() {
        return SSID;
    }

    public void setSSID(String SSID) {
        this.SSID = SSID;
    }

    public String getIp() {
        return Ip;
    }

    public void setIp(String ip) {
        Ip = ip;
    }
}
