package com.plv.drillwaymobile.models;

/**
 * Created by wpinango on 2/9/18.
 */

public class Theme {

    private int theme;
    private int themeType;
    private String themeString;

    public String getThemeString() {
        return themeString;
    }

    public void setThemeString(String themeString) {
        this.themeString = themeString;
    }

    public int getTheme() {
        return theme;
    }

    public void setTheme(int theme) {
        this.theme = theme;
    }

    public int getThemeType() {
        return themeType;
    }

    public void setThemeType(int themeType) {
        this.themeType = themeType;
    }
}
