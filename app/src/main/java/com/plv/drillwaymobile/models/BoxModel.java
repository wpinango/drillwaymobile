package com.plv.drillwaymobile.models;

import com.plv.drillwaymobile.Global;
import com.plv.drillwaymobile.dwmodels.Enum;

/**
 * Created by wpinango on 2/28/18.
 */

public class BoxModel {
    private String valueName;
    private Global.MobileNemonic nemonic;
    private String value;
    private String valueUnit;

    public String getValueName() {
        return valueName;
    }

    public void setValueName(String valueName) {
        this.valueName = valueName;
    }

    public Global.MobileNemonic getNemonic() {
        return nemonic;
    }

    public void setNemonic(Global.MobileNemonic nemonic) {
        this.nemonic = nemonic;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValueUnit() {
        return valueUnit;
    }

    public void setValueUnit(String valueUnit) {
        this.valueUnit = valueUnit;
    }
}
