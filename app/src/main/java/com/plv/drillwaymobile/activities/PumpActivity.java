package com.plv.drillwaymobile.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.plv.drillwaymobile.Asyntask;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.adapters.PumpListAdapter1;
import com.plv.drillwaymobile.dwmodels.Constant;
import com.plv.drillwaymobile.dwmodels.rigdata.DaqSetting;
import com.plv.drillwaymobile.dwmodels.rigdata.Pump;
import com.plv.drillwaymobile.dwmodels.rigdata.RigData;
import com.plv.drillwaymobile.interfaces.OnEditListener;
import com.plv.drillwaymobile.interfaces.OnTaskCompleted;
import com.plv.drillwaymobile.models.ServerInfo;
import com.plv.drillwaymobile.utils.Format;
import com.plv.drillwaymobile.utils.Formula;
import com.plv.drillwaymobile.utils.SharedPreferencesORM;

import java.util.ArrayList;

/**
 * Created by wpinango on 3/14/18.
 */

public class PumpActivity extends BaseNavigationActivity implements OnEditListener, OnTaskCompleted {
    private ArrayList<Pump>pumps = new ArrayList<>();
    private PumpListAdapter1 pumpListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Pumps");
        ListView lvPump = findViewById(R.id.lv_pumps);
        View vHeader = getLayoutInflater().inflate(R.layout.item_list_pump_header, null);
        lvPump.addHeaderView(vHeader);
        pumpListAdapter = new PumpListAdapter1(this, pumps, this);
        lvPump.setAdapter(pumpListAdapter);
        SharedPreferencesORM sharedPreferencesORM = new SharedPreferencesORM(this);
        ServerInfo serverInfo = sharedPreferencesORM.getServerInfo();
        if (pumps.isEmpty()) {
            new Asyntask.GetMethodAsynctask(this, Constant.EP_GET_RIG, Asyntask.testHeader, this, serverInfo.getAddress()).execute();
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_pump;
    }

    private void editPumpData(Pump pump, int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_pump_values, null);
        EditText etDiameter = view.findViewById(R.id.et_diameter_pump);
        EditText etLength = view.findViewById(R.id.et_length_pump);
        EditText etEfficient = view.findViewById(R.id.et_efficient_pump);
        TextView tvBbl = view.findViewById(R.id.tv_bbl_strk);
        TextView tvGal = view.findViewById(R.id.tv_gal_strk);
        tvBbl.setText(Format.getFormattedStringFourDecimal(Formula.calculateBbl(pump.getDiameter(),pump.getLength(),pump.getEfficient())));
        tvGal.setText(Formula.calculateBbl(pump.getDiameter(),pump.getLength(),pump.getEfficient()));
        etDiameter.setText(String.valueOf(pump.getDiameter()));
        etLength.setText(String.valueOf(pump.getLength()));
        etEfficient.setText(String.valueOf(pump.getEfficient()));
        builder.setTitle("Edit pump data");
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("Save", (dialog, which) -> {});
        builder.setNeutralButton("return", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public void onClickEditSource(DaqSetting source, int position) {

    }

    @Override
    public void onChangeStatusSource(boolean status, int position) {

    }

    @Override
    public void onClickEditPump(Pump pump, int position) {
        editPumpData(pump,position);
    }

    @Override
    public void onTaskCompleted(String type, String task) {
        if (type.equals(OnTaskCompleted.RIGDATA)){
            RigData rigData = new Gson().fromJson(task,RigData.class);
            pumps.addAll(rigData.getPumps());
            pumpListAdapter.notifyDataSetChanged();
        }
    }
}
