package com.plv.drillwaymobile.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.plv.drillwaymobile.Asyntask;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.adapters.SourceListAdapter;
import com.plv.drillwaymobile.adapters.SourceListAdapter1;
import com.plv.drillwaymobile.dwmodels.Constant;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.rigdata.DaqSetting;
import com.plv.drillwaymobile.dwmodels.rigdata.Pump;
import com.plv.drillwaymobile.dwmodels.rigdata.RigData;
import com.plv.drillwaymobile.interfaces.OnEditListener;
import com.plv.drillwaymobile.interfaces.OnTaskCompleted;
import com.plv.drillwaymobile.models.ServerInfo;
import com.plv.drillwaymobile.models.Source;
import com.plv.drillwaymobile.utils.SharedPreferencesORM;
import com.plv.drillwaymobile.utils.TransitionAnimation;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.plv.drillwaymobile.dwmodels.ValidateFields.getIpformat;
import static com.plv.drillwaymobile.dwmodels.ValidateFields.validate;

/**
 * Created by wpinango on 3/7/18.
 */

public class SourceActivity extends BaseNavigationActivity implements OnEditListener, OnTaskCompleted {
    //private Realm realm;
    private ArrayList<DaqSetting>sources = new ArrayList<>();
    private SourceListAdapter1 sourceListAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView();
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Source");
        //realm = Realm.getDefaultInstance();
        /*RealmResults<Source> sources = realm.where(Source.class).findAll();
        if (sources.size() == 0) {
            setSources();
        }*/
        //SourceListAdapter sourceListAdapter = new SourceListAdapter(this, sources, this);
        View vHeader = getLayoutInflater().inflate(R.layout.item_list_source_header, null);
        sourceListAdapter = new SourceListAdapter1(this,sources,this);
        ListView listView = findViewById(R.id.lv_source);
        listView.addHeaderView(vHeader);
        listView.setAdapter(sourceListAdapter);
        SharedPreferencesORM sharedPreferencesORM = new SharedPreferencesORM(this);
        ServerInfo serverInfo = sharedPreferencesORM.getServerInfo();
        if (sources.isEmpty()) {
            new Asyntask.GetMethodAsynctask(this, Constant.EP_GET_RIG, Asyntask.testHeader, this, serverInfo.getAddress()).execute();
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_sources;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //realm.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                TransitionAnimation.setOutActivityTransition(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TransitionAnimation.setOutActivityTransition(this);
    }

    /*private void setSources() {

        ArrayList<Source> sources = new ArrayList<>();
        Source source = new Source();
        source.setDevice("DBM");
        source.setId(UUID.randomUUID().toString());
        source.setPort("10002");
        source.setIp("192.168.0.99");
        sources.add(source);

        source = new Source();
        source.setDevice("DBS");
        source.setId(UUID.randomUUID().toString());
        source.setPort("10002");
        source.setIp("192.168.0.98");
        sources.add(source);

        for(Source s : sources){
            realm.beginTransaction();
            realm.copyToRealm(s);
            realm.commitTransaction();
        }

    }*/

    @Override
    public void onClickEditSource(DaqSetting source, int position) {
        editSource(source,position);
    }

    @Override
    public void onChangeStatusSource(boolean status, int position) {
        //changeSourceStatus(status,position);
    }

    @Override
    public void onClickEditPump(Pump pump, int position) {

    }

    private void editSource(DaqSetting source, int position){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_set_source, null);
        EditText etIP = view.findViewById(R.id.et_ip_source);
        EditText etPort = view.findViewById(R.id.et_port_source);
        EditText etSourceName = view.findViewById(R.id.et_name_source);
        RadioButton rbTCP = view.findViewById(R.id.rb_tcp);
        RadioButton rbUDP = view.findViewById(R.id.rb_udp);
        RadioGroup rgProtocol = view.findViewById(R.id.rg_protocol);
        etIP.setFilters(getIpformat());
        etSourceName.setText(source.getSource().name());
        etIP.setText(source.getHost());
        etPort.setText(String.valueOf(source.getPort()));
        if (source.getProtocol().equals(Enum.Protocol.TCP)){
            rgProtocol.check(R.id.rb_tcp);
        } else {
            rgProtocol.check(R.id.rb_udp);
        }
        builder.setTitle("Edit Source");
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton("Save", (dialog, which) -> {});
        builder.setNeutralButton("return", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate(etIP.getText().toString()) && etPort.getText().toString() != ""){
                    /*RealmResults<Source> s = realm.where(Source.class).findAll();
                    realm.beginTransaction();
                    s.get(position).setDevice(source.getDevice());
                    s.get(position).setPort(etPort.getText().toString());
                    s.get(position).setIp(etIP.getText().toString());
                    realm.commitTransaction();*/
                    alertDialog.cancel();
                } else {
                    etPort.setError("enter a valid port");
                    etIP.setError("enter a valid ip address");
                }
            }
        });
    }

    @Override
    public void onTaskCompleted(String type, String task) {
        //Log.d("valores" , task);
        if (type.equals(OnTaskCompleted.RIGDATA)){
            RigData rigData = new Gson().fromJson(task,RigData.class);
            Log.d("valores ", new Gson().toJson(rigData.getDaqSettings()));
            for (Map.Entry<Enum.Source,DaqSetting> daq : rigData.getDaqSettings().entrySet()) {
                sources.add(daq.getValue());
            }
            sourceListAdapter.notifyDataSetChanged();
        }
    }

    /*private void changeSourceStatus(boolean status, int position){
        RealmResults<Source> s = realm.where(Source.class).findAll();
        realm.beginTransaction();
        s.get(position).setStatus(status);
        realm.commitTransaction();
    }*/


}
