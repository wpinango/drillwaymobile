package com.plv.drillwaymobile.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.jraska.console.Console;
import com.plv.drillwaymobile.Global;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.User;
import com.plv.drillwaymobile.adapters.SectionPageAdapter;
import com.plv.drillwaymobile.dwmodels.rtdata.RtData;
import com.plv.drillwaymobile.interfaces.OnTaskCompleted;
import com.plv.drillwaymobile.models.BoxModel;
import com.plv.drillwaymobile.models.ServerInfo;
import com.plv.drillwaymobile.utils.RequestRTData;
import com.plv.drillwaymobile.utils.SharedPreferencesORM;
import com.plv.drillwaymobile.utils.Time;
import com.plv.drillwaymobile.utils.TransitionAnimation;

import java.io.IOException;
import java.io.InputStream;
import java.util.Timer;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import timber.log.Timber;

import static com.plv.drillwaymobile.dwmodels.ValidateFields.getIpformat;
import static com.plv.drillwaymobile.dwmodels.ValidateFields.validate;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnTaskCompleted {

    //private AppTheme theme;
    private Timer timer;
    private TextView tvServer;
    private ImageView imgResponseStatus;
    private SharedPreferencesORM sharedPreferencesORM;
    private RequestRTData requestRTData;
    private TextView tvWellName, tvRigName;
    private SectionPageAdapter sectionPageAdapter;
    private int timerValue = 2000;
    private ServerInfo serverInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //theme = new AppTheme(this);
        //setTheme((theme.getAppTheme()));
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        Toolbar toolbar1 = findViewById(R.id.toolbar);
        toolbar.setTitle("Drillway mobile");
        //theme.setToolbarColor(toolbar);
        //theme.setToolbarColor(toolbar1);
        sectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());
        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(sectionPageAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        //theme.setTabColors(tabLayout);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        Realm.init(this);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder()
                .name("drillwaydb.realm")
                .schemaVersion(0)
                .build();
        Realm.setDefaultConfiguration(realmConfig);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //theme.setNavigationViewColor(navigationView);
        toolbar.setNavigationIcon(R.drawable.pacific_32);
        sharedPreferencesORM = new SharedPreferencesORM(this);
        serverInfo = sharedPreferencesORM.getServerInfo();
        timer = new Timer();
        if (serverInfo.isConnect()) {
            requestRTData = new RequestRTData(this, this, serverInfo.getAddress());
            timer.schedule(requestRTData, 0, timerValue);
        }
        tvServer = findViewById(R.id.tv_server);
        imgResponseStatus = findViewById(R.id.img_response_status);
        tvRigName = findViewById(R.id.tv_rig_name);
        tvWellName = findViewById(R.id.tv_well_name);
        for (Global.MobileNemonic n : Global.MobileNemonic.values()) {
            BoxModel boxModel = new BoxModel();
            boxModel.setNemonic(n);
            Global.values.add(boxModel);
        }
        try {
            Global.user = new Gson().fromJson(loadJSONFromAsset(), User.class);
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public String loadJSONFromAsset() {
        String json;
        try {
            InputStream is = getAssets().open("userdefault.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == R.id.nav_source){
            Intent intent = new Intent(MainActivity.this,SourceActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }
        if (id == R.id.nav_theme){
            //theme.changeAppTheme();
            //recreate();
        }
        if (id == R.id.nav_pump) {
            Intent intent = new Intent(MainActivity.this,PumpActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }
        if (id == R.id.nav_mapping_channel) {
            Intent intent = new Intent(MainActivity.this,DevicesActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }
        if (id == R.id.nav_ping){
            Intent intent = new Intent(MainActivity.this,PingActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }
        if (id == R.id.nav_console){
            Intent intent = new Intent(MainActivity.this,ConsoleActivity.class);
            startActivity(intent);
            TransitionAnimation.setInActivityTransition(this);
        }
        if (id == R.id.nav_server){
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
            LayoutInflater inflater = this.getLayoutInflater();
            View view = inflater.inflate(R.layout.dialog_server_address, null);
            EditText text = view.findViewById(R.id.et_server_address);
            ImageView imgConnectionStatus = view.findViewById(R.id.img_connection_status);
            ServerInfo serverInfo = sharedPreferencesORM.getServerInfo();
            if (serverInfo.isConnect()) {
                imgConnectionStatus.setImageResource(R.drawable.connected);
            } else {
                imgConnectionStatus.setImageResource(R.drawable.disconnected);
            }
            text.setFilters(getIpformat());
            text.setText(serverInfo.getAddress());
            builder.setTitle("Server address");
            builder.setInverseBackgroundForced(true);
            builder.setView(view);
            builder.setCancelable(false);
            builder.setPositiveButton("Connect", (dialog, which) -> {
            });
            builder.setNeutralButton("Return", null);
            builder.setNegativeButton("Disconnect", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (timer != null && requestRTData != null) {
                        timer.cancel();
                        requestRTData.cancel();
                        requestRTData = null;
                    }
                    ServerInfo serverInfo = new ServerInfo();
                    serverInfo.setConnect(false);
                    serverInfo.setAddress(text.getText().toString());
                    sharedPreferencesORM.setServerInfo(serverInfo);
                    imgConnectionStatus.setImageResource(R.drawable.disconnected);
                    imgResponseStatus.setImageResource(R.drawable.offline);
                    tvServer.setText("Offline");
                }
            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (validate(text.getText().toString())){
                        ServerInfo serverInfo = new ServerInfo();
                        serverInfo.setAddress(text.getText().toString());
                        serverInfo.setConnect(true);
                        sharedPreferencesORM.setServerInfo(serverInfo);
                        requestRTData = new RequestRTData(MainActivity.this, MainActivity.this, serverInfo.getAddress());
                        if (timer == null) {
                            timer = new Timer();
                        }
                        timer.schedule(requestRTData, 0, timerValue);
                        imgConnectionStatus.setImageResource(R.drawable.connected);
                        alertDialog.cancel();
                    } else {
                        text.setError("enter a valid ip address");
                    }
                }
            });
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTaskCompleted(String type, String result) {
        try {
            if (type.equals(OnTaskCompleted.RTDATA)) {
                Global.rtData = new Gson().fromJson(result, RtData.class);
                tvWellName.setText(Global.rtData.getWellName());
                tvRigName.setText(Global.rtData.getRigName());
                sectionPageAdapter.updateValue();
                Console.writeLine(Time.getCurrentTime() + ": GetRTData");
                Timber.d("GetRTData:");
                Timber.w("Warning makes me nervous...");
                Timber.e("Some horrible ERROR!");
                Timber.wtf("WTF*!?!");
                tvServer.setText(serverInfo.getAddress());
                imgResponseStatus.setImageResource(R.drawable.online);
            }
            if (type.equals(OnTaskCompleted.RTOffLine)) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Console.writeLine(Time.getCurrentTime() + ": Error");
                        tvServer.setText("Offline");
                        imgResponseStatus.setImageResource(R.drawable.offline);
                    }
                });
            }
        }catch (Exception e) {
            e.getMessage();
        }
    }
}
