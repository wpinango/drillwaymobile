package com.plv.drillwaymobile.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.plv.drillwaymobile.Global;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.adapters.WifiListAdapter;
import com.plv.drillwaymobile.interfaces.OnConfigureIP;
import com.plv.drillwaymobile.interfaces.OnScanFinished;
import com.plv.drillwaymobile.utils.TransitionAnimation;
import com.plv.drillwaymobile.utils.WifiAdmin;
import com.plv.drillwaymobile.utils.WifiInformation;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.plv.drillwaymobile.dwmodels.ValidateFields.getIpformat;
import static com.plv.drillwaymobile.dwmodels.ValidateFields.validate;


/**
 * Created by wpinango on 2/14/18.
 */

public class WifiScanActivity extends AppCompatActivity implements OnScanFinished, OnConfigureIP {
    private List<ScanResult> wifiList = new ArrayList<>();
    private ListView lvWifi;
    private WifiInfo wifiInfo;
    private WifiListAdapter wifiListAdapter;
    private WifiAdmin wifiAdmin;
    private WifiInformation wifiInformation;
    private SweetAlertDialog sweetAlertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wifi_scan);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Select Pacific WifiData");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.pacific_32);
        getSupportActionBar().setDisplayUseLogoEnabled(true);
        wifiInformation = new WifiInformation(this);
        wifiAdmin = new WifiAdmin(this, this, this);
        wifiInfo = wifiInformation.getWifiInformation();
        lvWifi = findViewById(R.id.lv_wifi_scan);
        wifiAdmin.scanWifiList();
        wifiListAdapter = new WifiListAdapter(this,wifiList, wifiInfo);
        lvWifi.setAdapter(wifiListAdapter);
        ImageButton btnRefresh = findViewById(R.id.btn_refresh);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sweetAlertDialog = new SweetAlertDialog(WifiScanActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                sweetAlertDialog.setTitleText("Scanning network").getProgressHelper().setBarColor(R.color.blueDark);
                sweetAlertDialog.show();
                wifiAdmin.scanWifiList();
            }
        });
        lvWifi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    if (!wifiInfo.getBSSID().equals(wifiList.get(i).BSSID)) {
                        reviseNetworkSecurityToConnect(wifiList.get(i));
                    }
                }catch (Exception e) {
                    e.getMessage();
                }
            }
        });
        ImageButton btnDone = findViewById(R.id.btn_done_wifi);
        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               selectIpConfigWay();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        wifiAdmin.stopWifiServiceScanner();
    }

    @Override
    protected void onResume() {
        super.onResume();
        wifiAdmin.startWifiServiceScanner();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item_skip, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_skip:
                openMainActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void reviseNetworkSecurityToConnect(ScanResult scanResult) {
        if (scanResult.capabilities.contains("WPA") || scanResult.capabilities.contains("WEP")) {
            if (wifiAdmin.isNetworkExist(scanResult.SSID)) {
                wifiAdmin.connectToPreConfiguredWifi(scanResult);
            } else {
                connectToEncryptedNetwork(scanResult);
            }
        } else {
            connectToOpenNetwork(scanResult);
        }
    }

    private void connectToOpenNetwork(ScanResult scanResult){
        sweetAlertDialog = new SweetAlertDialog(WifiScanActivity.this , SweetAlertDialog.PROGRESS_TYPE);
        sweetAlertDialog.setTitleText("Connecting").getProgressHelper().setBarColor(R.color.blueDark);
        sweetAlertDialog.show();
        wifiAdmin.connectToOpenNetwork(scanResult);
    }

    private void connectToEncryptedNetwork(ScanResult scanResult) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_wifi_connect, null);
        TextView tvWifiSSID = view.findViewById(R.id.tv_network_ssid);
        EditText etWifiPass = view.findViewById(R.id.et_wifi_pass);
        etWifiPass.setTextColor(Color.BLACK);
        String networkSSID = scanResult.SSID;
        tvWifiSSID.setText(networkSSID);
        builder.setTitle("Connect to:");
        builder.setPositiveButton(R.string.connect, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String networkPass = etWifiPass.getText().toString();
                if (!networkPass.isEmpty()){
                    sweetAlertDialog = new SweetAlertDialog(WifiScanActivity.this , SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setTitleText("Connecting").getProgressHelper().setBarColor(R.color.blueDark);
                    sweetAlertDialog.show();
                    wifiAdmin.connectToEncryptedNetwork(scanResult, networkPass);
                } else {
                    TextInputLayout textInputLayout = view.findViewById(R.id.til_wifi_pass);
                    textInputLayout.setError("password empty");
                }
            }
        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void selectIpConfigWay(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_ip_box, null);
        RadioButton rbManual = view.findViewById(R.id.rb_ip_manual);
        RadioButton rbAutomatic = view.findViewById(R.id.rb_ip_automatic);
        builder.setTitle("Configure Ip");
        builder.setPositiveButton(R.string.connect, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (rbAutomatic.isChecked()) {
                    sweetAlertDialog = new SweetAlertDialog(WifiScanActivity.this , SweetAlertDialog.PROGRESS_TYPE);
                    sweetAlertDialog.setTitleText("Configuring Network").getProgressHelper().setBarColor(R.color.blueDark);
                    sweetAlertDialog.show();
                    wifiAdmin.scanNetwork();
                    //openMainActivity();
                }  else if (rbManual.isChecked()) {
                    setManualIp();
                } else {
                    Global.Toaster.get().showToast(WifiScanActivity.this,"Nothing selected"
                            , Toast.LENGTH_SHORT);
                }
            }
        });
        builder.setView(view);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void setManualIp() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_set_ip, null);
        EditText text = view.findViewById(R.id.et_ip_address);
        text.setTextColor(Color.BLACK);
        text.setFilters(getIpformat());
        builder.setTitle("Set ip");
        builder.setView(view);
        builder.setPositiveButton("Done", (dialog, which) -> {
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate(text.getText().toString())){
                    String hostname = text.getText().toString();
                    System.out.println("nstask : " + hostname);
                    new WifiAdmin.NetworkSniffTask(WifiScanActivity.this, WifiScanActivity.this)
                        .setWifiStaticIP(hostname);
                } else {
                    text.setError("enter a valid ip address");
                }
            }
        });
    }

    private void openMainActivity(){
        Intent intent = new Intent(WifiScanActivity.this, MainActivity.class);
        startActivity(intent);
        TransitionAnimation.setInActivityTransition(this);
        finish();
    }

    @Override
    public void onResult(List<ScanResult> list, WifiInfo wifi) {
        wifiListAdapter.updateWifiInfo(wifi);
        wifiInfo = wifi;
        wifiList.clear();
        wifiList.addAll(list);
        wifiListAdapter.notifyDataSetChanged();
        if (sweetAlertDialog != null) {
            sweetAlertDialog.cancel();
        }
    }


    @Override
    public void onConfigureIPFinish(String SSID, String IP) {
        //System.out.println("valores 2.1: " + SSID + " " + IP);
        if (!SSID.equals("") && !IP.equals("")) {
            Global.Toaster.get().showToast(WifiScanActivity.this, "Ip configured: " +
                    IP, Toast.LENGTH_SHORT);
            wifiInformation.saveWifiData(SSID,IP);
        } else {
            Global.Toaster.get().showToast(WifiScanActivity.this,"Something went worn",
                    Toast.LENGTH_SHORT);
        }
        openMainActivity();
        if (sweetAlertDialog != null) {
            sweetAlertDialog.cancel();
        }
    }
}
