package com.plv.drillwaymobile.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.util.Log;

import com.google.gson.Gson;
import com.plv.drillwaymobile.models.WifiData;
import com.plv.drillwaymobile.utils.SharedPreferences;
import com.plv.drillwaymobile.utils.WifiAdmin;
import com.plv.drillwaymobile.utils.WifiInformation;

/**
 * Created by wpinango on 2/16/18.
 */

public class SplashActivity extends AppCompatActivity {
    Intent intent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WifiInformation wifiInformation = new WifiInformation(this);
        WifiData wifiData = wifiInformation.getWifiData();
        try {
            //System.out.println("valores : " + wifiData.getSSID() + " " + wifiInformation.getWifiInformation().getSSID());
            if (wifiData.getSSID().equals(wifiInformation.getWifiInformation().getSSID()) &&
                    wifiInformation.getCurrentNetworkIP() != 0) {
                intent = new Intent(this, MainActivity.class);
            } else {
                intent = new Intent(this, WifiScanActivity.class);
            }
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.getMessage();
        }
    }
}