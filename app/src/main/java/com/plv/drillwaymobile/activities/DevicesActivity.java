package com.plv.drillwaymobile.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.plv.drillwaymobile.Asyntask;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.adapters.DeviceListAdapter;
import com.plv.drillwaymobile.adapters.DeviceListAdapter1;
import com.plv.drillwaymobile.dwmodels.Constant;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.rigdata.DaqSetting;
import com.plv.drillwaymobile.dwmodels.rigdata.RigData;
import com.plv.drillwaymobile.dwmodels.rtdata.RtData;
import com.plv.drillwaymobile.interfaces.OnTaskCompleted;
import com.plv.drillwaymobile.models.ServerInfo;
import com.plv.drillwaymobile.utils.SharedPreferencesORM;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by wpinango on 3/12/18.
 */

public class DevicesActivity extends BaseNavigationActivity implements OnTaskCompleted {
    private DeviceListAdapter1 deviceListAdapter;
    private ArrayList<DaqSetting> sources = new ArrayList<>();
    private RigData rigData;
    //private ArrayList<>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setSubtitle("Devices");
        deviceListAdapter = new DeviceListAdapter1(this,rigData);
        ExpandableListView lvDevices = findViewById(R.id.lv_devices);
        View vHeader = getLayoutInflater().inflate(R.layout.item_list_device_header, null);
        lvDevices.addHeaderView(vHeader);
        lvDevices.setAdapter(deviceListAdapter);
        SharedPreferencesORM sharedPreferencesORM = new SharedPreferencesORM(this);
        ServerInfo serverInfo = sharedPreferencesORM.getServerInfo();
        if (sources.isEmpty()) {
            new Asyntask.GetMethodAsynctask(this, Constant.EP_GET_RTDATA, Asyntask.testHeader, this, serverInfo.getAddress()).execute();
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_channels;
    }

    @Override
    public void onTaskCompleted(String type, String task) {
        if (type.equals(OnTaskCompleted.RIGDATA)){
            rigData = new Gson().fromJson(task,RigData.class);
            //RigData rigData = new Gson().fromJson(task,RigData.class);
            /*for (Map.Entry<Enum.Source,DaqSetting> daq : rigData.getDaqSettings().entrySet()) {
                sources.add(daq.getValue());
            }*/

            deviceListAdapter.notifyDataSetChanged();
        }
    }
}
