package com.plv.drillwaymobile.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ToggleButton;

import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.adapters.PingListAdapter;
import com.plv.drillwaymobile.utils.TransitionAnimation;
import com.stealthcopter.networktools.Ping;
import com.stealthcopter.networktools.ping.PingResult;
import com.stealthcopter.networktools.ping.PingStats;

import java.net.UnknownHostException;
import java.util.ArrayList;

import static com.plv.drillwaymobile.dwmodels.ValidateFields.getIpformat;

/**
 * Created by wpinango on 2/27/18.
 */

public class PingActivity extends BaseNavigationActivity {
    private EditText etPing;
    private ToggleButton btnOp;
    private PingListAdapter pingListAdapter;
    private ArrayList<String>pingResults = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setSubtitle("ping to address");
        pingListAdapter = new PingListAdapter(this,pingResults);
        ListView lvPing = findViewById(R.id.lv_ping);
        lvPing.setAdapter(pingListAdapter);
        etPing = findViewById(R.id.tv_ping_result);
        etPing.setText("192.168.0.10");
        btnOp = findViewById(R.id.btn_ping);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        etPing.setFilters(getIpformat());
        btnOp.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    try {
                        Ping.onAddress(etPing.getText().toString()).setTimeOutMillis(1000).setTimes(5).doPing(new Ping.PingListener() {
                            @Override
                            public void onResult(PingResult pingResult) {
                                if (pingResult.fullString != null) {
                                    pingResults.add(pingResult.fullString.split("\\n\\n")[0]);
                                } else {
                                    pingResults.add("Destination host is unreachable");
                                }
                            }
                            @Override
                            public void onFinished(PingStats pingStats) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        btnOp.setChecked(false);
                                        pingListAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_ping;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_item_delete, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_delete){
            pingResults.clear();
            pingListAdapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }
}
