package com.plv.drillwaymobile.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.plv.drillwaymobile.Global;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.adapters.NumericGridAdapter;
import com.plv.drillwaymobile.models.BoxModel;

import java.util.ArrayList;

/**
 * Created by wpinango on 2/7/18.
 */

public class NumericFragment extends Fragment {
    private NumericGridAdapter numericGridAdapter;
    //private ArrayList<BoxModel> values = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_numeric,null);
        numericGridAdapter = new NumericGridAdapter(getActivity(), Global.values);
        GridView gridNumeric = view.findViewById(R.id.grid_numeric);
        gridNumeric.setAdapter(numericGridAdapter);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void setValues(ArrayList<BoxModel> values) {
        //this.values.addAll(values);
        //System.out.println("valores 4 " + new Gson().toJson(values));
        numericGridAdapter.notifyDataSetChanged();
    }

    public void updateValues() {
        numericGridAdapter.notifyDataSetChanged();
    }

}
