package com.plv.drillwaymobile;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.widget.Toast;

import com.plv.drillwaymobile.dwmodels.rtdata.RtData;
import com.plv.drillwaymobile.models.BoxModel;

import java.util.ArrayList;

/**
 * Created by wpinango on 2/15/18.
 */

public class Global {

    public static RtData rtData;
    public static ArrayList<BoxModel> values = new ArrayList<>();
    public static User user;
    public static String ipAddress;

    public static boolean connectToServer = false;

    public enum Toaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> Toast.makeText(context, message, length).show()
                );
            }
        }

        public static Toaster get() {
            return INSTANCE;
        }
    }

    public enum CenterToaster {
        INSTANCE;
        private final Handler handler = new Handler(Looper.getMainLooper());

        public void showToast(final Context context, final String message, final int length) {
            if (context != null) {
                handler.post(
                        () -> {
                            Toast toast = Toast.makeText(context, message, length);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                );
            }
        }

        public static CenterToaster get() {
            return INSTANCE;
        }
    }

    public static enum MobileNemonic {

        HOOKLOAD,
        PUMPPRESURE,
        FLOWOUT

    }
}
