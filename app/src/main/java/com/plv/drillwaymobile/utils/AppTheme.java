package com.plv.drillwaymobile.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;

import com.google.gson.Gson;
import com.plv.drillwaymobile.R;
import com.plv.drillwaymobile.models.Theme;

/**
 * Created by wpinango on 2/9/18.
 */

public class AppTheme {

    private static String lightTheme = "light";
    private static String darkTheme = "dark";
    private SharedPreferences sharedPreferences;
    private Context context;

    public AppTheme(Context context) {
        this.context = context;
    }

    public int getAppTheme(){
        int defaultTheme = R.style.LightAppTheme_NoActionBar;
        sharedPreferences = new SharedPreferences(context);
        try {
            String t = sharedPreferences.getSavedActivityTheme();
            if (!t.equals("")) {
                Theme theme = new Gson().fromJson(t, Theme.class);
                defaultTheme = theme.getTheme();
            }
        } catch (Exception e){
            e.getMessage();
        }
        return defaultTheme;
    }


    private void setAppTheme(int theme, String themeString) {
        sharedPreferences = new SharedPreferences(context);
        Theme th = new Theme();
        th.setTheme(theme);
        th.setThemeString(themeString);
        sharedPreferences.saveActivityTheme(new Gson().toJson(th));
    }

    public void changeAppTheme(){
        if (getAppTheme() == R.style.LightAppTheme_NoActionBar){
            setAppTheme(R.style.DarkAppTheme_NoActionBar, AppTheme.darkTheme);
        } else {
            setAppTheme(R.style.LightAppTheme_NoActionBar, AppTheme.lightTheme);
        }
    }

    public void setTabColors(TabLayout tabLayout) {
        if (getAppTheme() == R.style.LightAppTheme_NoActionBar){
            tabLayout.setTabTextColors(ContextCompat.getColorStateList(context,R.color.blue));
            //tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(context, R.color));
            tabLayout.setBackgroundResource(R.color.whiteGray);
        } else {
            tabLayout.setBackgroundResource(R.color.blue);
            tabLayout.setTabTextColors(ContextCompat.getColorStateList(context,R.color.whiteGray));
            tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(context, R.color.white));
        }
    }

    public void setToolbarColor(Toolbar toolbar) {
        if (getAppTheme() == R.style.LightAppTheme_NoActionBar){
            toolbar.setBackgroundResource(R.color.whiteGray);
            toolbar.setTitleTextColor(Color.BLACK);
        } else {
            toolbar.setBackgroundResource(R.color.blue);
            toolbar.setTitleTextColor(Color.WHITE);
        }
    }

    public void setNavigationViewColor(NavigationView navigationView) {
        if (getAppTheme() == R.style.LightAppTheme_NoActionBar){
            navigationView.setItemTextColor(ColorStateList.valueOf(Color.BLACK));
            navigationView.setBackgroundResource(R.color.white);
        } else {
            navigationView.setItemTextColor(ColorStateList.valueOf(Color.WHITE));
            navigationView.setBackgroundResource(R.color.blueDark);
        }
    }
}
