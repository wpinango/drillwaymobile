package com.plv.drillwaymobile.utils;

import android.content.Context;

import com.plv.drillwaymobile.Asyntask;
import com.plv.drillwaymobile.dwmodels.Constant;
import com.plv.drillwaymobile.interfaces.OnTaskCompleted;

import java.util.TimerTask;

/**
 * Created by wpinango on 2/27/18.
 */

public class RequestRTData extends TimerTask {
    private Context context;
    private OnTaskCompleted onTaskCompleted;
    private String address;

    public RequestRTData(Context context, OnTaskCompleted onTaskCompleted, String address) {
        this.context = context;
        this.onTaskCompleted = onTaskCompleted;
        this.address = address;
    }

    @Override
    public void run() {
        if (context != null) {
            new Asyntask.GetMethodAsynctask(context, Constant.EP_GET_RTDATA, Asyntask.testHeader, onTaskCompleted,address).execute();
        }
    }
}
