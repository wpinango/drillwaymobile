package com.plv.drillwaymobile.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import com.google.gson.Gson;
import com.plv.drillwaymobile.models.WifiData;

/**
 * Created by wpinango on 2/26/18.
 */

public class WifiInformation {

    private Context context;
    private WifiManager wifiManager;
    private ConnectivityManager connectivityManager;

    public WifiInformation(Context context){
        this.context = context;
        this.wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        this.connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public WifiInfo getWifiInformation() {
        return wifiManager.getConnectionInfo();
    }

    public int getCurrentNetworkIP() {
        return wifiManager.getConnectionInfo().getIpAddress();
    }

    public boolean isWifiConnected() {
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return networkInfo.isConnected();
    }

    public void saveWifiData(String SSID, String IP) {
        //System.out.println("valores  2 : " + SSID + " " + IP);
        SharedPreferences sharedPreferences = new SharedPreferences(context);
        WifiData wifiData = new WifiData();
        wifiData.setIp(IP);
        wifiData.setSSID(SSID);
        sharedPreferences.saveWifiData(new Gson().toJson(wifiData));
    }

    public WifiData getWifiData(){
        SharedPreferences sharedPreferences = new SharedPreferences(context);
        WifiData wifiData = new WifiData();
        if (sharedPreferences.getSavedNetwork().equals("")){
            wifiData.setIp("127.0.0.1");
            wifiData.setSSID("Nothing");
        } else {
            wifiData = new Gson().fromJson(sharedPreferences.getSavedNetwork(),WifiData.class);
        }
        return wifiData;
    }
}
