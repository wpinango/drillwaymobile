package com.plv.drillwaymobile.utils;

import android.app.Activity;

/**
 * Created by wpinango on 2/8/18.
 */

public class Util {

    public static void applySharedTheme(Activity activity, int theme) {
        activity.setTheme(theme);
    }
}
