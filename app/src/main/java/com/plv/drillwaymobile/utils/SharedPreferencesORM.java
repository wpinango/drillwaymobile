package com.plv.drillwaymobile.utils;

import android.content.Context;

import com.google.gson.Gson;
import com.plv.drillwaymobile.models.ServerInfo;

/**
 * Created by wpinango on 3/2/18.
 */

public class SharedPreferencesORM {
    private Context context;
    private SharedPreferences sharedPreferences;

    public SharedPreferencesORM(Context context){
        this.context = context;
        this.sharedPreferences = new SharedPreferences(context);
    }

    public void setServerInfo(ServerInfo serverInfo){
        sharedPreferences.saveServerInfo(new Gson().toJson(serverInfo));
    }

    public ServerInfo getServerInfo(){
        ServerInfo serverInfo = new ServerInfo();
        try {
            if (sharedPreferences.getServerInfo() == "") {
                serverInfo.setConnect(false);
                serverInfo.setAddress("127.0.0.1");
            } else {
                serverInfo = new Gson().fromJson(sharedPreferences.getServerInfo(),ServerInfo.class);
            }
        } catch (Exception e ){
            e.getMessage();
        }
        return serverInfo;
    }

}
