package com.plv.drillwaymobile.utils;

import java.text.DecimalFormat;

/**
 * Created by wpinango on 3/22/18.
 */

public class Format {

    public static String getFormattedStringTwoDecimal(String value) {
        return new DecimalFormat("0.0").format(Float.valueOf(value));
    }

    public static String getFormattedStringFourDecimal(String value) {
        return new DecimalFormat("0.0000").format(Float.valueOf(value));
    }
}
