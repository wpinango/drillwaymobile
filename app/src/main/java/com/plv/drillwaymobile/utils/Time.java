package com.plv.drillwaymobile.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by wpinango on 3/2/18.
 */

public class Time {

    private static Calendar getCurrentCalendar() {
        return Calendar.getInstance();
    }


    public static String getCurrentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return (sdf.format(getCurrentCalendar().getTime()));
    }
}
