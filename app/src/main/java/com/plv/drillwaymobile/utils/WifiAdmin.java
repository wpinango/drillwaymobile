package com.plv.drillwaymobile.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.plv.drillwaymobile.Global;
import com.plv.drillwaymobile.interfaces.OnConfigureIP;
import com.plv.drillwaymobile.interfaces.OnScanFinished;

import java.lang.ref.WeakReference;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wpinango on 2/19/18.
 */

public class WifiAdmin {
    private Context context;
    private OnScanFinished listener;
    private OnConfigureIP onConfigureIP;
    private WifiManager wifiManager;
    private List<ScanResult> wifiList = new ArrayList<>();
    private WifiInfo wifiInfo;


    public WifiAdmin(Context context, OnScanFinished listener, OnConfigureIP onConfigureIP) {
        this.context = context;
        this.listener = listener;
        this.onConfigureIP = onConfigureIP;
        this.context.registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
        this.wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
    }

    private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context c, Intent intent) {
            try {
                if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                    wifiInfo = wifiManager.getConnectionInfo();
                    List<ScanResult> scanResults = wifiManager.getScanResults();
                    wifiList.clear();
                    LinkedHashMap<String, ScanResult> map = new LinkedHashMap<>();
                    for (ScanResult scanResult : scanResults) {
                        if (scanResult.SSID != null && !scanResult.SSID.isEmpty()) {
                            map.put(scanResult.SSID, scanResult);
                        }
                    }
                    List<ScanResult> sortedWifiList = new ArrayList<ScanResult>(map.values());
                    Comparator<ScanResult> comparator = new Comparator<ScanResult>() {
                        @Override
                        public int compare(ScanResult lhs, ScanResult rhs) {
                            return Integer.valueOf(rhs.level).compareTo(lhs.level);//(lhs.level < rhs.level ? -1 : (lhs.level == rhs.level ? 0 : 1));
                        }
                    };
                    Collections.sort(sortedWifiList, comparator);
                    for (ScanResult scanResult : sortedWifiList) {
                        if (!map.containsKey(scanResult.SSID)) {
                            map.put(scanResult.SSID, scanResult);
                        }
                    }
                    for (Map.Entry<String, ScanResult> sr : map.entrySet()) {
                        wifiList.add(sr.getValue());
                    }
                    listener.onResult(wifiList, wifiInfo);
                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
    };

    public void startWifiServiceScanner() {
        context.registerReceiver(mWifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
    }

    public void stopWifiServiceScanner() {
        context.unregisterReceiver(mWifiScanReceiver);
    }

    public void scanWifiList() {
        if (!wifiManager.isWifiEnabled()) {
            Global.Toaster.get().showToast(context, "wifi is disable, making it enabled", Toast.LENGTH_SHORT);
            wifiManager.setWifiEnabled(true);
        }
        wifiManager.startScan();
    }

    private void connectToWifi(WifiConfiguration wifiConfig) {
        if (isNetworkExist(wifiConfig.SSID)) {
            wifiManager.disconnect();
            wifiManager.enableNetwork(getConfiguredNetwork(wifiConfig.SSID), true);
            wifiManager.reconnect();
        } else {
            int networkId = wifiManager.addNetwork(wifiConfig);
            wifiManager.disconnect();
            wifiManager.enableNetwork(networkId, true);
            wifiManager.reconnect();
        }
        scanWifiList();
    }

    public void connectToPreConfiguredWifi(ScanResult scanResult) {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = "\"" + scanResult.SSID + "\"";
        connectToWifi(wifiConfig);
    }

    private int getConfiguredNetwork(String SSID) {
        SSID = SSID.replaceAll("\"", "");
        for (WifiConfiguration config : wifiManager.getConfiguredNetworks()) {
            String newSSID = config.SSID.replaceAll("\"", "");
            if (SSID.equals(newSSID)) {
                return config.networkId;
            }
        }
        return 0;
    }

    public boolean isNetworkExist(String SSID) {
        SSID = SSID.replaceAll("\"", "");
        for (WifiConfiguration config : wifiManager.getConfiguredNetworks()) {
            String newSSID = config.SSID.replaceAll("\"", "");
            if (SSID.equals(newSSID)) {
                return true;
            }
        }
        return false;
    }

    public void scanNetwork() {
        new NetworkSniffTask(context, onConfigureIP).execute();
    }

    public void connectToOpenNetwork(ScanResult scanResult) {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = "\"" + scanResult.SSID + "\"";
        wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
        connectToWifi(wifiConfig);
    }

    public void connectToEncryptedNetwork(ScanResult scanResult, String networkPass) {
        WifiConfiguration wifiConfig = new WifiConfiguration();
        wifiConfig.SSID = "\"" + scanResult.SSID + "\"";
        if (scanResult.capabilities.contains("WPA")) {
            wifiConfig.preSharedKey = "\"" + networkPass + "\"";
        } else if (scanResult.capabilities.contains("WEP")) {
            wifiConfig.wepKeys[0] = "\"" + networkPass + "\"";
            wifiConfig.wepTxKeyIndex = 0;
            wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
            wifiConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
        }
        connectToWifi(wifiConfig);
    }

    public static class NetworkSniffTask extends AsyncTask<Void, Void, Void> {

        private static final String TAG = "nstask";
        private WeakReference<Context> mContextRef;
        private WifiManager wifiManager;
        private WifiConfiguration wifiConfig;
        private OnConfigureIP onConfigureIP;

        public NetworkSniffTask(Context context, OnConfigureIP onConfigureIP) {
            mContextRef = new WeakReference<>(context);
            this.onConfigureIP = onConfigureIP;
            this.wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
            for (WifiConfiguration conf : configuredNetworks) {
                if (conf.networkId == connectionInfo.getNetworkId()) {
                    this.wifiConfig = conf;
                    break;
                }
            }
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "Let's sniff the network");

            try {
                Context context = mContextRef.get();

                if (context != null) {

                    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                    WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

                    WifiInfo connectionInfo = wm.getConnectionInfo();
                    int ipAddress = connectionInfo.getIpAddress();
                    String ipString = Formatter.formatIpAddress(ipAddress);

                    Log.d(TAG, "activeNetwork: " + String.valueOf(activeNetwork));
                    Log.d(TAG, "ipString: " + String.valueOf(ipString));

                    String prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1);
                    Log.d(TAG, "prefix: " + prefix);

                    for (int i = 255; i > 0; i--) {
                        String testIp = prefix + String.valueOf(i);

                        InetAddress address = InetAddress.getByName(testIp);
                        boolean reachable = address.isReachable(1000);
                        String hostName = address.getCanonicalHostName();

                        if (reachable) {
                            Log.i(TAG, "Host: " + String.valueOf(hostName) + "(" + String.valueOf(testIp) + ") is reachable!");
                        } else {
                            setWifiStaticIP(hostName);
                            break;
                        }
                    }
                }
            } catch (Throwable t) {
                Log.e(TAG, "Well that's not good.", t);
            }

            return null;
        }

        public void setWifiStaticIP(String hostName){
            try {
                setIpAssignment("STATIC", wifiConfig); //or "DHCP" for dynamic setting
                Log.d("nstask", wifiConfig + " ");
                setIpAddress(InetAddress.getByName(String.valueOf(hostName)), 24, wifiConfig);
                setGateway(InetAddress.getByName("4.4.4.4"), wifiConfig);
                setDNS(InetAddress.getByName("4.4.4.4"), wifiConfig);
                wifiManager.updateNetwork(wifiConfig); //apply the setting
                wifiManager.saveConfiguration(); //Save it
                Log.d(TAG, (hostName) + " aqui la info : " + wifiConfig);
                //System.out.println("valores : pase " + wifiConfig.SSID);
            } catch (Exception e) {
                e.getMessage();
            }
            onConfigureIP.onConfigureIPFinish(wifiConfig.SSID, String.valueOf(hostName));
        }

        static void setIpAssignment(String assign, WifiConfiguration wifiConf)
                throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
            setEnumField(wifiConf, assign, "ipAssignment");
        }

        static void setIpAddress(InetAddress addr, int prefixLength, WifiConfiguration wifiConf)
                throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
                NoSuchMethodException, ClassNotFoundException, InstantiationException, InvocationTargetException {
            Object linkProperties = getField(wifiConf, "linkProperties");
            if (linkProperties == null) return;
            Class laClass = Class.forName("android.net.LinkAddress");
            Constructor laConstructor = laClass.getConstructor(new Class[]{InetAddress.class, int.class});
            Object linkAddress = laConstructor.newInstance(addr, prefixLength);

            ArrayList mLinkAddresses = (ArrayList) getDeclaredField(linkProperties, "mLinkAddresses");
            mLinkAddresses.clear();
            mLinkAddresses.add(linkAddress);
        }

        static void setGateway(InetAddress gateway, WifiConfiguration wifiConf)
                throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException,
                ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException {
            Object linkProperties = getField(wifiConf, "linkProperties");
            if (linkProperties == null) return;
            Class routeInfoClass = Class.forName("android.net.RouteInfo");
            Constructor routeInfoConstructor = routeInfoClass.getConstructor(new Class[]{InetAddress.class});
            Object routeInfo = routeInfoConstructor.newInstance(gateway);

            ArrayList mRoutes = (ArrayList) getDeclaredField(linkProperties, "mRoutes");
            mRoutes.clear();
            mRoutes.add(routeInfo);
        }

        static void setDNS(InetAddress dns, WifiConfiguration wifiConf)
                throws SecurityException, IllegalArgumentException, NoSuchFieldException, IllegalAccessException {
            Object linkProperties = getField(wifiConf, "linkProperties");
            if (linkProperties == null) return;

            ArrayList<InetAddress> mDnses = (ArrayList<InetAddress>) getDeclaredField(linkProperties, "mDnses");
            mDnses.clear(); //or add a new dns address , here I just want to replace DNS1
            mDnses.add(dns);
        }

        static Object getField(Object obj, String name)
                throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
            Field f = obj.getClass().getField(name);
            Object out = f.get(obj);
            return out;
        }

        static Object getDeclaredField(Object obj, String name)
                throws SecurityException, NoSuchFieldException,
                IllegalArgumentException, IllegalAccessException {
            Field f = obj.getClass().getDeclaredField(name);
            f.setAccessible(true);
            Object out = f.get(obj);
            return out;
        }

        private static void setEnumField(Object obj, String value, String name)
                throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
            Field f = obj.getClass().getField(name);
            f.set(obj, Enum.valueOf((Class<Enum>) f.getType(), value));
        }
    }
}
