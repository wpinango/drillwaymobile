package com.plv.drillwaymobile.utils;

import android.content.Context;

import com.plv.drillwaymobile.R;

/**
 * Created by wpinango on 2/9/18.
 */

public class SharedPreferences {
    private Context context;
    private String KEY_BACKGROUND = "BackgroundColor";
    private String KEY_COLOR = "color";
    private String KEY_WIFI = "currentWifi";
    private String KEY_NETWORK = "NetworkInfo";
    private String KEY_SERVER = "server";
    private String KEY_SERVER_INFO = "serverInfo";

    SharedPreferences(Context context){
        this.context = context;
    }

    public void saveActivityTheme(String theme){
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_BACKGROUND, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_COLOR, theme);
        editor.apply();
    }

    public String getSavedActivityTheme(){
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_BACKGROUND,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_COLOR, "");
    }

    public void saveWifiData(String network) {
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_WIFI, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_NETWORK, network);
        editor.apply();
    }

    public String getSavedNetwork(){
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_WIFI,Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_NETWORK, "");
    }

    public void saveServerInfo(String serverInfo){
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_SERVER, Context.MODE_PRIVATE);
        android.content.SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_SERVER_INFO,serverInfo);
        editor.apply();
    }

    public String getServerInfo(){
        android.content.SharedPreferences sharedPreferences = context.getSharedPreferences(KEY_SERVER, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_SERVER_INFO, "");
    }
}
