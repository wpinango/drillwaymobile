/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.variable;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
public class Calibration  implements Cloneable{


    private boolean status;
    private ArrayList<CalibrationData> data;

    
    
    protected Object clone() {
        Calibration obj = null;
        
        try {
            obj =  (Calibration) super.clone(); //To change body of generated methods, choose Tools | Templates.
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Calibration.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < obj.data.size(); i++) {
            obj.data.set(i, (CalibrationData) obj.data.get(i).clone());
        }
        return obj;
    }

    
    
    
    public Calibration() {
        data = new ArrayList<>();
        //todo borrar el siguiente codigo
        data.add(new CalibrationData());
        data.add(new CalibrationData());
    }

    public ArrayList<CalibrationData> getData() {
        return data;
    }

    public CalibrationData getLastData() {
        return data.get(data.size() - 1);
    }

    public CalibrationData getData(int index) {
        return data.get(index);
    }

    public void setData(ArrayList<CalibrationData> data) {
        this.data = data;
    }

    public void setData(CalibrationData data, int index) {
        this.data.set(index, data);
    }

    public void setData(float value, float voltage, int index) {
        this.data.get(index).value = value;
        this.data.get(index).voltage = voltage;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
