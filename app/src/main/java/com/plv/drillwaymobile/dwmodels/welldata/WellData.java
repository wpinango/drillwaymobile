/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.welldata;

import com.google.gson.Gson;

/**
 *
 * @author JoseAntonio
 */
public class WellData {
    private itemLAS wellName;
    private itemLAS company;
    private itemLAS field;
    private itemLAS location;
    private itemLAS service;
    private itemLAS country;
    private itemLAS date;
    private itemLAS latitud;
    private itemLAS longitud;
    private itemLAS start;
    private itemLAS stop;
    private itemLAS step;
    private itemLAS NULL; 
    private String dbaseName;

    public WellData() {
      wellName=   new itemLAS(); 
      company=new itemLAS();
      field=new itemLAS();
      location=new itemLAS();
      service=new itemLAS();
      country=new itemLAS();
      date=new itemLAS();
      latitud=new itemLAS();
      longitud=new itemLAS();
      start=new itemLAS();
      stop=new itemLAS();
      step=new itemLAS();
      NULL= new itemLAS();
    }

    public String toJson(){
        return new Gson().toJson(this);
    }
    
    
    
    
    public String getDBaseName() {
        return dbaseName;
    }

    public void setDBaseName(String DBaseName) {
        this.dbaseName = DBaseName;
    }
    

    public void setWellName(String name) {
        this.wellName.value = name;
    }

    public void setCompany(String company) {
        this.company.value = company;
    }

    public void setField(String field) {
        this.field.value = field;
    }

    public void setLocation(String location) {
        this.location.value = location;
    }

    public void setService(String service) {
        this.service.value = service;
    }

    public void setCountry(String country) {
        this.country.value = country;
    }

    public void setDate(String date) {
        this.date.value = date;
    }

    public void setLatitud(String latitud) {
        this.latitud.value = latitud;
    }

    public void setLongitud(String longitud) {
        this.longitud.value = longitud;
    }

    public void setStart(String start) {
        this.start.value = start;
    }

    public void setStop(String stop) {
        this.stop.value = stop;
    }

    public void setStep(String step) {
        this.step.value = step;
    }

    public itemLAS getWellName() {
        return wellName;
    }

    public itemLAS getCompany() {
        return company;
    }

    public itemLAS getField() {
        return field;
    }

    public itemLAS getLocation() {
        return location;
    }

    public itemLAS getService() {
        return service;
    }

    public itemLAS getCountry() {
        return country;
    }

    public itemLAS getDate() {
        return date;
    }

    public itemLAS getLatitud() {
        return latitud;
    }

    public itemLAS getLongitud() {
        return longitud;
    }

    public itemLAS getStart() {
        return start;
    }

    public itemLAS getStop() {
        return stop;
    }

    public itemLAS getStep() {
        return step;
    }

    public itemLAS getNULL() {
        return NULL;
    }

    /*public Document getDocument() {
        return Document.parse(new Gson().toJson(this));
    }*/ // TODO averiguar

  



   
    
    
    
    
    
}
