/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.hydraulics;

import java.util.ArrayList;

/**
 *
 * @author JoseAntonio
 */
public class BitData {

    public ArrayList<Float> nozzles = new ArrayList<>();
    public float gpm;
    public float mudWh;
    public float coefficient;
    public float diameter;
    ///////////////////////////////////////////////////
    public float TFA;
    public float nozzleVelocity;
    public float pressDrop;
    public float HPTotal;
    public float HPsi;
    public float impactForce;

    public BitData(float diameter,float gpm, float mudWh, float coefficient, ArrayList<Float> nozzle) {
        this.diameter = diameter;
        this.nozzles = nozzle;
        this.gpm = gpm;
        this.mudWh = mudWh;
        this.coefficient = coefficient;
        calcTFA();
        calcVelocity();
        calcPressDrop();
        calcHPtotal();
        calcHPsi();
        calcImpactForce();
    }
        public BitData(float diameter,float gpm, float mudWh, float coefficient, float tfa) {
        this.diameter = diameter;
        this.TFA = tfa;
        this.gpm = gpm;
        this.mudWh = mudWh;
        this.coefficient = coefficient;
        calcVelocity();
        calcPressDrop();
        calcHPtotal();
        calcHPsi();
        calcImpactForce();
    }

    @Override
    public String toString() {
        return "BitData{" + "TFA=" + TFA + ", nozzleVelocity=" + nozzleVelocity + ", pressDrop=" + pressDrop + ", HPTotal=" + HPTotal + ", HPsi=" + HPsi + ", impactForce=" + impactForce + '}';
    }

    private void calcTFA() {
        double temp = 0;
        temp = nozzles.stream().map((nozzle) -> Math.pow(nozzle/32, 2)).reduce(temp,
                (accumulator, _item) -> accumulator + _item);
        this.TFA =  (float) ((Math.PI/4)*temp);
    }

    private void calcVelocity() {
        nozzleVelocity = (float) (gpm / (3.117*TFA));
    }
    
    private void calcPressDrop(){
        pressDrop = (float) ((Math.pow(gpm, 2)*mudWh)/
                (12032*Math.pow(coefficient, 2)*Math.pow(TFA, 2)));
    }
    
    private void calcHPtotal(){
        HPTotal = (gpm*pressDrop)/1714;
    }

    private void calcHPsi(){
        HPsi = (float) (HPTotal/(0.7854f*Math.pow(diameter, 2)));
    }
    
    private void calcImpactForce(){
        impactForce = (gpm*mudWh*nozzleVelocity)/1932;
    }
    
}
