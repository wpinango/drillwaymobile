/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.Mongo;

/*import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;

import static com.mongodb.client.model.Filters.*;

import static com.mongodb.client.model.Indexes.descending;
import com.mongodb.client.model.UpdateOptions;
import java.util.ArrayList;
import java.util.List;
import org.bson.BSON;
import org.bson.Document;
import org.bson.conversions.Bson;*/

/**
 *
 * @author JoseAntonio
 */
public class MongoDb {
    /*private MongoClient mongoClient = null;
    private MongoDatabase mongoDatabase = null;
    private MongoCollection mongoCollection = null;

    *//**
     *
     * @param user
     * @param pass
     * @param host
     * @param port
     * @param databaseName
     *
     *
     *//*
    public MongoDb(String user, String pass, String host, int port, String databaseName) {
        char[] pwd = pass.toCharArray();
        MongoCredential credential = MongoCredential.createCredential(user, databaseName, pwd);
        List<MongoCredential> credList = new ArrayList();
        credList.add(credential);
        ServerAddress address = new ServerAddress(host, port);
        mongoClient = new MongoClient(address, credList);
        //mongoClient = new MongoClient(address);
    }

//    *//**
//     * Establece Una Coleccion
//     *
//     * @param collection
//     *//*
//    public synchronized void setCollection(String collection) {
//        mongoCollection = mongoDatabase.getCollection(collection);
//    }

    public MongoDatabase getDatabase(String database) {
        return mongoClient.getDatabase(database);
    }

    *//**
     * Inserta un solo Documento en la coleccion establecida
     *
     * @param document
     *//*
    public void insertOneDocument(String database,String collection ,Document document) {
        try {
            mongoClient.getDatabase(database).getCollection(collection).insertOne(document);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
        *//**
     * Inserta o remplaza un solo Documento en la coleccion establecida
     *
     * @param database
     * @param collection
     * @param document
     * @param field
     *//*
    public void insertOrReplaceDocument(String database,String collection ,Document document,String field) {
        try {
            mongoClient.getDatabase(database)
                    .getCollection(collection)
                    .replaceOne(eq(field, document.getInteger(field)),document,new UpdateOptions().upsert(true));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    

    public void insertOneDocument(String database,String collection ,String json) {
        mongoClient.getDatabase(database).getCollection(collection).insertOne(Document.parse(json));

    }
    
    public void replaceFirstDocument(String database,String collection ,String json) {
      if (mongoClient.getDatabase(database).getCollection(collection).find().first() == null){
          mongoClient.getDatabase(database).getCollection(collection).insertOne(Document.parse(json));
      }else{
        mongoClient.getDatabase(database).getCollection(collection).replaceOne(
                (Document) mongoClient.getDatabase(database).getCollection(collection)
                        .find().first(), Document.parse(json),
                new UpdateOptions().upsert(true));
      }
    }

    *//**
     * Inserta una lista de Documentos en la colecion establecida
     *
     * @param documents
     *//*
    public void insertManyDocument(String database,String collection ,List<Document> documents) {
        mongoClient.getDatabase(database).getCollection(collection).insertMany(documents);
    }

    *//**
     * Retorna el conteo total de documentos de la collecion establecida
     *
     * @param database
     * @param collection
     * @return
     *//*
    public long getSizeCollection(String database,String collection) {
        return mongoClient.getDatabase(database).getCollection(collection).count();
    }

    *//**
     * Retorna el primer documento de la coleccion establecida
     *
     * @param database
     * @param collection
     * @return Documento tipo Document
     *//*
    public Document getFirstDocument(String database,String collection) {
        return mongoClient.getDatabase(database).getCollection(collection).find().first();
    }

    public Document getLastDocument(String database,String collection) {
        FindIterable<Document> iterable = mongoClient.getDatabase(database)
                .getCollection(collection).find().limit(1).sort(descending("_id"));
        return iterable.first();
    }

    public FindIterable<Document> getDocumentBetwen(String database,String collection ,
            String field1, Object value1, Object value2) {
        FindIterable<Document> iterable = mongoClient.getDatabase(database).getCollection(collection).find(and(gte(field1, value1), lte(field1, value2))).sort(descending(field1));
        return iterable;
    }
    
    public FindIterable<Document> getDocumentBetwen(String database,String collection ,
            String field1, Object value1, Object value2,int mods) {
        FindIterable<Document> iterable = mongoClient.getDatabase(database)
                .getCollection(collection)
                .find(and(gte(field1, value1), lte(field1, value2),mod(field1, mods, 0)))
                .sort(descending(field1));
        return iterable;
    }
    

//    public void setDatabase(String database) {
//        mongoDatabase = mongoClient.getDatabase(database);
//    }

//    public String getDatabaseName(String database) {
//        return mongoDatabase.getName();
//    }

    public void createCollection(String database,String collection) {
        mongoDatabase.createCollection(collection);
    }

    public ArrayList<String> getListDatabasesNames() {
        MongoIterable<String> nameList = mongoClient.listDatabaseNames();

        ArrayList<String> tempList = new ArrayList<>();
        MongoCursor cursor = nameList.iterator();
        while (cursor.hasNext()) {
            tempList.add(cursor.next().toString());
        }
        return tempList;
    }

    public void updateDocument2 (String database,String collection ,Bson filter, Bson update) {
        mongoClient.getDatabase(database).getCollection(collection).updateOne(filter, update,new UpdateOptions().upsert(true));
    }

    public void replaceDocument(String database,String collection ,Document document, Document document0) {
        mongoClient.getDatabase(database).getCollection(collection).replaceOne(document, document0, new UpdateOptions().upsert(true));

    }
    
    public void replaceDocument(String database,String collection ,Bson filter, Document replacement) {
        mongoClient.getDatabase(database).getCollection(collection).replaceOne(filter, replacement, new UpdateOptions().upsert(true));

    }

    public boolean iscollectionExists(String database,final String collectionName) {
        MongoIterable<String> collectionNames = mongoDatabase.listCollectionNames();
        for (final String name : collectionNames) {
            if (name.equalsIgnoreCase(collectionName)) {
                return true;
            }
        }
        return false;
    }

    public boolean isdbaseExists(final String dbaseName) {
        MongoIterable<String> collectionNames = mongoClient.listDatabaseNames();
        for (final String name : collectionNames) {
            if (name.equalsIgnoreCase(dbaseName)) {
                return true;
            }
        }
        return false;
    }   */
}
