/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels;



/**
 *
 * @author JoseAntonio
 */
public class Unit {
    public String name;
    public String description;
    public double factor;

    public Unit(String name, String description, double value) {
        this.name = name;
        this.description = description;
        this.factor = value;
    }
    
    @Override
    public String toString(){
        return name  + ", " + description  + ", " + factor;
    }
    
    
}
