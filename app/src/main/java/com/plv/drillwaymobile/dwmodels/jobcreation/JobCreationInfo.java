/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.jobcreation;

import com.google.gson.Gson;
import com.plv.drillwaymobile.dwmodels.rigdata.RigData;

import java.util.ArrayList;

/**
 *
 * @author JoseAntonio
 */
public class JobCreationInfo {

    private ArrayList<String> DbaseNames;
    private ArrayList<RigData> rigDatas;
    // private RigConfig jobConfig;
    //private String jobConfigName;

//    public String getRigConfigName() {
//        return jobConfigName;
//    }
//
//    public void setJobConfigName(String jobConfigName) {
//        this.jobConfigName = jobConfigName;
//    }
//    public RigConfig getJobConfig() {
//        return jobConfig;
//    }
//
//    public void setJobConfig(RigConfig jobConfig) {
//        this.jobConfig = jobConfig;
//    }
    public ArrayList<RigData> getRigDatas() {
        return rigDatas;
    }

    public RigData getRigData(int index) {
        return rigDatas.get(index);
    }
    
    public void addRigData(RigData rigData){
        rigDatas.add(rigData);
    }

    public void setRigDatas(ArrayList<RigData> rigDatas) {
        this.rigDatas = rigDatas;
    }

    public ArrayList<String> getDBaseNames() {
        return DbaseNames;
    }

    public void setDBaseNames(ArrayList<String> actualJobs) {
        this.DbaseNames = actualJobs;
    }

    public JobCreationInfo() {
        DbaseNames = new ArrayList<>();
        rigDatas = new ArrayList<>();
    }
    
       public String toJson(){
        return new Gson().toJson(this);
    }

}
