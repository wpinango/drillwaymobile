/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.variable;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
     public class CalibrationData implements Cloneable{
        public double voltage;
        public double value;

    
    protected Object clone() {
        CalibrationData obj = null;
            try {
                obj =  (CalibrationData) super.clone(); //To change body of generated methods, choose Tools | Templates.
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(CalibrationData.class.getName()).log(Level.SEVERE, null, ex);
            }
            return obj;
    }
        
    }
