/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.welldata;

/**
 *
 * @author JoseAntonio
 */
public class itemLAS {

    public String nemonic;
    public String description;
    public String value;

    public itemLAS() {
        nemonic = "";
        description = "";
        value = "";
    }

    @Override
    public String toString() {
        return value;
    }

}
