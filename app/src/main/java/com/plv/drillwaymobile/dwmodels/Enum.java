/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels;

/**
 *
 * @author JoseAntonio
 */

public class Enum {

    public static enum nemonic {

    }

    public static enum TypeScreen {

        DBASE, NUMERIC, CHART, NUME_CHART_V, NUME_CHART_H, NUME_NUME_V, NUME_NUME_H, CHART_CHART_V;
    }

    public static enum TypeDataBox {

        NUMERIC_CHART, GAUGE, LEVEL;
    }

    public static enum BlockDir {

        UP, DOWN;
    }

    public static enum BitStatus {

        ONBT, OFFBT;
    }

    public static enum DrillingStrings {

        Drill_Pipe, Drill_Collar, Heavy_Weight,
    }

    public static enum RigActivity {

        UNDEFINED_STATUS,
        RIG_UP_DOWN,
        DRILLING,
        CONNECTION,
        REAMING,
        HOLE_OPENING,
        CORING,
        CIRCULATE_MUD,
        TRIPPING_IN,
        TRIPPING_OUT,
        LUBRICATE_RIG,
        RIG_REPAIR,
        CUT_DRILLING_LINE,
        SURVEY,
        WIRELINE,
        RUN_CASING,
        CEMENTING,
        PLUG_BACK,
        SQUEEZE_CEMENTING,
        WAIT_ON_CEMENT,
        DRILL_CEMENT,
        NIPPLE_DOWN_BOP,
        TEST_BOP,
        DRILL_STEM_TEST,
        FISHING,
        DIRECTIONAL_WORK,
        WELL_CONTROL,
        STUCK_PIPE,
        WAIT_ON_WEATHER,
        SUBSEA,
        FLOW_CHECK,
        PRESSURE_INTEGRITY_TEST,
        LOST_CIRCULATION,
        SHORT_TRIP_IN,
        SHORT_TRIP_OUT;
    }

    public static enum TypeIndexChart {

        TIME, DEPTH, REAM, TVD;
    }

    public static enum SlipStatus {

        OUT, IN;
    }

    public static enum SourceStatus {

        DISABLE, ENABLED, CONECTED;

    }

    public static enum VarType {

        ANALOG, RATE, COUNTER, FORMULA, COMMENT, OTHER;
    }

    public static enum Source {

        BDM, BDS, CHROMA, WITS1, WITS2, RTD, GAS;
    }

    public static enum Protocol {
        UDP, TCP, SERIAL;
    }

    public static enum UnitFamily {

        ANGLE,
        AREA,
        CATION_EACH_MAP,
        CATION_EXCHANGE,
        CONCENTRATION,
        CONDUCTIVITY,
        COST_RATE,
        CURRENT,
        DATA_RATE,
        DENSITY,
        DOGLEG_SEVERITY,
        DRILLING_COST,
        GRAVITY_FIELD,
        LENGHT_WEIGTH,
        LENGTH,
        OTHER,
        POWER,
        POWER_AREA,
        PRESSURE,
        PRESSURE_GRADNT,
        RATE,
        STROKE_RATE,
        TEMPERATURE,
        TIME,
        TIME_LENGTH,
        UNITS,
        VELOCITY,
        VISCOSITY,
        VISCOSITY_PLASTIC,
        VOLTAGE,
        VOLUME,
        VOLUME_LENGTH,
        VOLUME_RATE,
        WEIGHT,
        WEIGHT_LENGTH,
        WORK,
        YIELD_POINT
    }

    public static enum MNemonic {

        HOOKLOAD,
        PUMPPRESURE,
        FLOWOUT,
        GASTOTAL1,
        GASTOTAL2,
        CASINGPR,
        PITTRIP1,
        PITTRIP2,
        PIT01,
        PIT02,
        PIT03,
        PIT04,
        PIT05,
        PIT06,
        PIT07,
        PIT08,
        PIT09,
        PIT10,
        MUDGL,
        MUDIN,
        MUDOUT,
        TEMPIN,
        TEMPOUT,
        CONDIN,
        CONDOUT,
        CO21,
        CO22,
        H2S1,
        H2S2,
        H2S3,
        CH41,
        CH42,
        CH43,
        TORQUEE,
        TORQUEH,
        RPMROT,
        RPMUP,
        RPMBOTTON,
        SPM1,
        SPM2,
        SPM3,
        SPM4,
        SPM5,
        SPM6,
        STK1,
        STK2,
        STK3,
        STK4,
        STK5,
        STK6,
        GPM1,
        GPM2,
        GPM3,
        GPM4,
        GPM5,
        GPM6,
        SPMTOT,
        STKTOT,
        GPMTOT,
        PVTOTAL,
        RPMTOTAL,
        RMDEPTH,
        BITDEPTH,
        MSDEPTH,
        TVDEPTH,
        BLOCKDIR,
        BLOCKPOS,
        BLOCKMOVE,
        WONBIT,
        WONBITAVG,
        ROP,
        ROPI,
        ROPX5,
        MINUND,
        TIMEEFEDRILL,
        TIMEROT,
        TIMESLIP,
        TIMECIRC,
        STDIN,
        STDOUT,
        STDTOTAL,
        TRIPSPEED,
        TONNMILES,
        DIFPRESURE,
        C1,
        C2,
        C3,
        iC4,
        nC4,
        iC5,
        nC5,
        LAGDEPTH,
        LAGSTKS,
        LAGTIME,
        DXC,
        POREPR,
        FRACT,
        VANULAR,
        VTOTHOLE,
        DTOTAL,
        SLIPSTATUS,
        SLIPCOUNT,
        ACTIVITY,
        ECD,
        COMMENT1,
        COMMENT2,
        COMMENT3,
        SPARE1,
        SPARE2,
        SPARE3,
        SPARE4,
        SPARE5,
        SPARE6,
        SPARE7,
        SPARE8,
        SPARE9,
        SPARE10,
        SPARE11,
        SPARE12,
        SPARE13,
        SPARE14,
        SPARE15,
        SPARE16,
        SPARE17,
        SPARE18,
        SPARE19,
        SPARE20,
        SPARE21,
        SPARE22,
        SPARE23,
        SPARE24,
        SPARE25,
        SPARE26,
        SPARE27,
        SPARE28,
        SPARE29,
        SPARE30,
        SPARE31,
        SPARE32,
        SPARE33,
        SPARE34,
        SPARE35,
        SPARE36,
        SPARE37,
        SPARE38,
        SPARE39,
        SPARE40,
        SPARE41,
        SPARE42,
        SPARE43,
        SPARE44,
        SPARE45,
        SPARE46,
        SPARE47,
        SPARE48,
        SPARE49,
        SPARE50;
    }

}
