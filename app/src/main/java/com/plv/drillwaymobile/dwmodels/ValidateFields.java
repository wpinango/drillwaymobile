/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels;

/*import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextField;*/

import android.text.InputFilter;
import android.text.Spanned;
import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author sosadp [abc] “a”, “b”, o “c” (simple) [^abc] Cualquier carácter
 * excepto “a”, “b”, o “c” (negación) [a-zA-Z] De la “a” a la “z”, o de la “A” a
 * la “Z”, ambas inclusive(rango) [a-d[m-p]] De la “a” a la “d”, o de la “m” a
 * la “p”: [a-dm-p] (union) [a-z&&[def]] “d”, “e”, o “f” (intersección)
 * [a-z&&[^bc]] “a” a la “z”, excepto “b” y “c”: [ad-z] (resta) [a-z&&[^m-p]]
 * “a” a la “z”, pero no de la “m” a la “p”: [a-lq-z] (resta)
 *
 * Y algunos atajos. . Cualquier carácter. \d Un dígito: [0-9] \D Dígitos no:
 * [^0-9] \s Un espacio en blanco: [ \t\n\x0B\f\r] \S Cualquiera excepto un
 * espacio en blanco: [^\s] \w Un carácter alfanumérico: [a-zA-Z_0-9] \W Un
 * carácter no de palabra: [^\w]
 *
 *
 */
public class ValidateFields {

    private static final String PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public static boolean validate(final String ip){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }

    public static InputFilter[] getIpformat() {
        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end)
                            + destTxt.substring(dend);
                    if (!resultingTxt.matches ("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                        return "";
                    } else {
                        String[] splits = resultingTxt.split("\\.");
                        for (String split : splits) {
                            if (Integer.valueOf(split) > 255) {
                                return "";
                            }
                        }
                    }
                }
                return null;
            }
        };
        return filters;
    }

    /*private char vacio = 0;
    private String o = "|";
    private String webHost = "^(http|https)\\://[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\\-\\._\\?\\,\\'/\\\\\\+&%\\$#\\=~])*$";
    private String n255 = "([1-9]?[0-9]|1[0-9]{2}|2([0-4][0-9]|5[0-5]))";
    private String l255 = (n255 + "[.]|" + n255 + "[.]" + n255);
    private String o255 = (l255 + "[.]|" + n255 + "[.]" + n255 + "[.]" + n255);
    private String p255 = (o255 + "[.]|" + n255 + "[.]" + n255 + "[.]" + n255 + "[.]" + n255);

    private Pattern webHostPattern = Pattern.compile(webHost);

    private Pattern dirIp = Pattern.compile(n255 + o + l255 + o + o255 + o + p255);
    private Pattern extendedDirIp = Pattern.compile("^(http|https)\\://(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"); //"^(http|https)\\://"+n255 + o + l255 + o + o255 + o + p255);
    
    private Pattern numerico = Pattern.compile("[0-9]*");
//    //^(\-?\d+(\.\d+)?),\s*(\-?\d+(\.\d+)?)$
//    private Pattern coordenada = Pattern.compile("//^(\\-?\\d+(\\.\\d+)?),\\s*(\\-?\\d+(\\.\\d+)?)$");
    private Pattern coordenada = Pattern.compile("[[-+][0-9][.][0-9][,][-+][0-9][.][0-9]]");
    private Pattern txt = Pattern.compile("[a-zA-Z]");
    private Pattern alfaNum = Pattern.compile("[a-zA-Z[-]0-9]");
    private Pattern port = Pattern.compile("^(6553[0-5]|655[0-2][0-9]|65[0-4][0-9]{2}|6[0-4][0-9]{3}|[0-5]?([0-9]){0,3}[0-9])$");
    private Pattern finalHost = Pattern.compile("([1-9]?[0-9]|1[0-9]{2}|2([0-4][0-9]|5[0-5]))([.]([1-9]?[0-9]|1[0-9]{2}|2([0-4][0-9]|5[0-5]))){3}");

    public Boolean validateNum(KeyEvent pEvt) {

        Matcher matcher = numerico.matcher((String.valueOf(pEvt.getKeyChar())));
        if (!matcher.matches()) {
            pEvt.setKeyChar(vacio);
            return false;
        } else {
            return true;
        }
    }

    public Boolean validateText(KeyEvent pEvt) {
        Matcher matcher = txt.matcher((String.valueOf(pEvt.getKeyChar())));
        if (!matcher.matches()) {
            pEvt.setKeyChar(vacio);
            return false;
        } else {
            return true;
        }
    }

    public Boolean validateAlfaNum(KeyEvent pEvt) {
        Matcher matcher = alfaNum.matcher((String.valueOf(pEvt.getKeyChar())));
        if (!matcher.matches()) {
            pEvt.setKeyChar(vacio);
            return false;
        } else {
            return true;
        }

    }

    public Boolean validateIpAdress(JTextField field, KeyEvent pEvt) {

        Matcher matcher = dirIp.matcher(field.getText() + String.valueOf(pEvt.getKeyChar()));
        if (!matcher.matches()) {
            pEvt.setKeyChar(vacio);
            return false;
        } else {
            return true;
        }
    }

    public Boolean validatePort(JTextField field, KeyEvent pEvt) {

        Matcher matcher = port.matcher(field.getText() + String.valueOf(pEvt.getKeyChar()));
        if (!matcher.matches()) {
            pEvt.setKeyChar(vacio);
            return false;
        } else {
            return true;
        }

    }

    public Boolean validateCoordenada(KeyEvent pEvt) {
        Matcher matcher = coordenada.matcher((String.valueOf(pEvt.getKeyChar())));
        if (!matcher.matches()) {
            pEvt.setKeyChar(vacio);
            return false;
        } else {
            return true;
        }

    }

    public Boolean validateDirIp(KeyEvent pEvt) {
        Matcher matcher = dirIp.matcher((String.valueOf(pEvt.getKeyChar())));
        if (!matcher.matches()) {
            pEvt.setKeyChar(vacio);
            return false;
        } else {
            return true;
        }
    }

    public Boolean isvalidHost(String host) {
        return finalHost.matcher(host).matches();
    }

    public boolean isValidHostAndIpAnddress(String host) {
        Matcher matcher1 = webHostPattern.matcher(host);
        Matcher matcher2 = extendedDirIp.matcher(host);
        return matcher1.matches() || matcher2.matches();
    }*/

}
