/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.jobcreation;

import com.google.gson.Gson;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.rigdata.RigData;
import com.plv.drillwaymobile.dwmodels.variable.DataLocal;
import com.plv.drillwaymobile.dwmodels.welldata.WellData;

import java.util.LinkedHashMap;

/**
 *
 * @author JoseAntonio
 */
public class JobCreationModel {

    //private String DbName;
   // private String jobConfigName;
    private RigData rigData;
    private WellData wellData;
    private LinkedHashMap<Enum.MNemonic, DataLocal> vardataLocal;
   // private LinkedHashMap<com.plogging.dwmodels.Enum.MNemonic, DataMaster> vardataMaster;

    public JobCreationModel(RigData rigData, WellData wellData,
            LinkedHashMap<Enum.MNemonic, DataLocal> vardataLocal) {
        //this.DbName = DbName;
       // this.jobConfigName = jobConfigName;
        this.rigData = rigData;
        this.wellData = wellData;
        this.vardataLocal = vardataLocal;
        //this.vardataMaster = vardataMaster;
    }

//    public LinkedHashMap<Enum.MNemonic, DataMaster> getVardataMaster() {
//        return vardataMaster;
//    }
//
//    public void setVardataMaster(LinkedHashMap<Enum.MNemonic, DataMaster> vardataMaster) {
//        this.vardataMaster = vardataMaster;
//    }

//    public String getDbName() {
//        return DbName;
//    }
//
//    public void setDbName(String DbName) {
//        this.DbName = DbName;
//    }
//
//    public String getJobConfigName() {
//        return jobConfigName;
//    }
//
//    public void setJobConfigName(String jobConfigName) {
//        this.jobConfigName = jobConfigName;
//    }

    public RigData getRigData() {
        return rigData;
    }

    public void setWellData(WellData wellData) {
        this.wellData = wellData;
    }

    public WellData getWellData() {
        return wellData;
    }

    public void setRigConfig(RigData jobConfig) {
        this.rigData = jobConfig;
    }

    public LinkedHashMap<Enum.MNemonic, DataLocal> getVardatalocal() {
        return vardataLocal;
    }

    public void setVardatalocal(LinkedHashMap<Enum.MNemonic, DataLocal> vardatalocal) {
        this.vardataLocal = vardatalocal;
    }
    public String toJson(){
        return new Gson().toJson(this);
    }

}
