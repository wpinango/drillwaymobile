/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rigdata;

import com.google.gson.Gson;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Jose Palumbo
 */
public class Encoder implements Cloneable {

    public int pulsesRevolution;
    public int drumLaps;
    public float drumPerimeter;
    public int blockLines;
    public int drumLapsRoll;
    public float lineDiameter;
    public float minHeight;

    @Override
    public Object clone() {
        Encoder obj = null;
        try {
            obj = (Encoder) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(RigData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    
//    private static String join(String[] parts, String delim) {
//        StringBuilder result = new StringBuilder();
//        
//        for (int i = 0; i < parts.length; i++) {
//            String part = parts[i];
//            result.append(part);
//            if (delim != null && i < parts.length-1) {
//                result.append(delim);
//            }        
//        }
//        return result.toString();
//    }

    public String toJson() {
        return new Gson().toJson(this);
    }

}
