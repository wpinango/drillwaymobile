/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels;

import static com.plv.drillwaymobile.dwmodels.Enum.MNemonic.*;

/**
 *
 * @author JoseAntonio
 */
public class Constant {

    //////////////////////SOCKET////////////////////////////////////////////////
    public static final String ID_SOCKET_OBJET = "id";
    public static final String BODY_SOCKET_OBJET = "data";
    public static final String RT_SOCKET_OBJET = "RT";
    public static final String VARDATALOCAL_SOCKET_GET = "VarDataLocal";
    public static final String VARDATALOCAL_SOCKET_UPDATE = "VarDataLocalUpdate";
    public static final String RIGDATA_SOCKET_GET = "RigData";
    public static final String RIGDATA_SOCKET_UPDATE = "RigDataUpdate";
    public static final String RIGDATA_SOCKET_NEW = "RigDataNew";
    public static final String RIGDATA_SOCKET_REMOVE = "RigDataRemove";
    public static final String RIGDATALIST_SOCKET_GET = "RigDataList";
    public static final String JOBDATALIST_SOCKET_GET = "JobDataList";
    public static final String WELLDATA_SOCKET_GET = "WellData";
    public static final String WELLDATA_SOCKET_UPDATE = "WellDataUpdate";
    public static final String DAQCONFIG_SOCKET_UPDATE = "DaqConfigUpdate";
    public static final String OK_SOCKET = "OK";
    public static final String NO_SOCKET = "NO";
    public static final String BLOCK_CALIBRATION_GET = "GetBlockCalibration";
    public static final String BLOCK_CALIBRATION_SET = "SetBlockCalibration";
    public static final String RTDATATIME_SOCKET_GET = "rtDataTimeGet";
    public static final String RTDATATIME_SOCKET_PUT = "rtDataTimePut";
    public static final String RTDATADEPTH_SOCKET_GET = "rtDataDepthGet";
    public static final String RTDATADBASE_SOCKET_GET = "rtDataDBaseGet";
    public static final String RTDATADEPTH_SOCKET_PUT = "rtDataDepthPut";
    public static final String VARVAL_SOCKET_UPDATE = "VARVAL";
    public static final String OKINITDATA_SOCKET_GET = "okInitData";
    public static final String CREATE_DBASE_SOCKET = "createDBase";
    public static final String JOBCREATION_SOCKET_GET = "JobCreationGet";
    public static final String JOBCREATION_SOCKET_EVENTS = "JobCreationEvents";
    public static final String JOBCREATION_SOCKET_FINAL = "JobCreationFinal";
    public static final String JOBCREATION_SOCKET_ERROR = "JobCreationError";
    public static final String WITSCONFIGS_SOCKET_UPDATE = "WitsConfigsUpdate";
    public static final String WITSCONFIGS_SOCKET_GET = "WitsConfigsGet";
    public static final String WITSCONFIG_SOCKET_UPDATE = "WitsConfigUpdate";

    //////////////////////////PATH CONTS////////////////////////////////////////
    public static final String PATH_SETTING = "Setting";
    public static final String PATH_USER = "User";
    public static final String PATH_RIGS = "Rigs";
    public static final String FILE_MAINCONFIG = "mainconfig.json";
    public static final String FILE_JOBCONFIG = "jobconfig.json";
    public static final String FILE_RIGDATA = "rigdata.json";
    public static final String FILE_LISTPIPES = "listpipes.json";
    public static final String FILE_GLOBALUNIT = "globalunit.json";

    ////////////////////////DBASE CONTS/////////////////////////////////////////
    public static final String COLEC_RTDATA = "rtdata";
    public static final String COLEC_TIME = "timedata";
    public static final String COLEC_DEPTH = "depthdata";
    public static final String COLEC_RIGDATA = "rigdata";
    public static final String COLEC_WELLDATA = "welldata";
    public static final String COLEC_VARDATALOCAL = "vardatalocal";
    public static final String COLEC_LAGDATA = "lagdata";
    public static final String FIELD_TIMESTAMP = "timestamp";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_MSDEPTH = Enum.MNemonic.MSDEPTH.toString();

    public static final Enum.MNemonic SPMs[] = {SPM1, SPM2, SPM3, SPM4, SPM5, SPM6};
    public static final Enum.MNemonic STKs[] = {STK1, STK2, STK3, STK4, STK5, STK6};
    public static final Enum.MNemonic GPMs[] = {GPM1, GPM2, GPM3, GPM4, GPM5, GPM6};
    public static final Enum.MNemonic PITs[] = {PIT01, PIT02, PIT03, PIT04, PIT05, PIT06,
        PIT07, PIT08, PIT09, PIT10, PITTRIP1, PITTRIP2};

    ///////////////////////////END POINT////////////////////////////////////////
    public static final String EP_UPDATE_DAQ = "/updatedaqsetting";
    public static final String EP_UPDATE_ENCODER = "/updateencodersetting";
    public static final String EP_GET_ENCODER = "/getencodersetting";
    public static final String EP_UPDATE_PULSES_HEIGTH = "/updatepulseshegth";
    public static final String EP_GET_PULSES_HEIGTH = "/getpulseshegth";
    public static final String EP_MAKEJOB = "/makejob";
    public static final String EP_GET_JOB_LIST = "/getjoblist";
    public static final String EP_REMOVE_JOB = "/removejob";
    public static final String EP_GET_RTDATA = "/getrtData";
    public static final String EP_GET_FIRST_RECORD_TIME = "/getfirstrecordtime";
    public static final String EP_GET_LAST_RECORD_DEPTH = "/getlastdepth";
    public static final String EP_GET_LAG_DATA = "/getlagdata";
    public static final String EP_UPDATE_VAR = "/updatevarvalue";
    public static final String EP_INSERT_COMMENT_TIME = "/insertcommenttime";
    public static final String EP_INSERT_COMMENT_DEPTH = "/insertcommentdepth";
    public static final String EP_UPDATE_RIG = "/updaterigdata";
    public static final String EP_GET_RIG = "/getrigdata";
    public static final String EP_GET_RIG_LIST = "/getrigdatalist";
    public static final String EP_INSERT_RIG = "/insertrigdata";
    public static final String EP_REMOVE_RIG = "/removerigdata";
    public static final String EP_GET_TIME = "/gettimedata";
    public static final String EP_GET_TIME_MAP = "/gettimedatamap";
    public static final String EP_GET_DEPTH = "/getdepthdata";
    public static final String EP_GET_DEPTH_MAP = "/getdepthdatamap";
    public static final String EP_UPDATE_WELL = "/updatewelldata";
    public static final String EP_GET_WELL = "/getwelldata";
    public static final String EP_GET_WITS = "/getwitsdata";
    public static final String EP_UPDATE_WITS = "/updatewitsdata";
    public static final String EP_UPDATE_WITS_LIST = "/updatewitsdatalist";

}
