/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.LagData;

/**
 *
 * @author JoseAntonio
 */
public class Sample {
    private int depth;
    private int stroke;
   

    public Sample(int depth, int strokeOut) {
        this.depth = depth;
        this.stroke = strokeOut;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public int getStrokeOut() {
        return stroke;
    }

    public void setStrokeOut(int strokeOut) {
        this.stroke = strokeOut;
    }
    
    
}
