/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rigdata;

/**
 *
 * @author JoseAntonio
 */
public class Casing {


    private String name;
    private double OD;
    private double ID;
    private double length;
    private double startDepth;
    private double endDepth;
    //private float endDepthC;
    private double weight;
    private double drift;

    public Casing(String name) {
        this.name = name;
    }
    
    
    
//    public float getEndDepthC() {
//        return endDepthC;
//    }
//
//    public void setEndDepthC(float endDepthC) {
//        this.endDepthC = endDepthC;
//    }

    
    public double getDrift() {
        return drift;
    }

    public void setDrift(double drift) {
        this.drift = drift;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }
    
    

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getID() {
        return OD;
    }

    public void setID(double diameterIn) {
        this.OD = diameterIn;
    }

    public double getOD() {
        return ID;
    }

    public void setOD(double diameerOut) {
        this.ID = diameerOut;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getStartDepth() {
        return startDepth;
    }

    public void setStartDepth(double startDepth) {
        this.startDepth = startDepth;
    }

    public double getEndDepth() {
        return endDepth;
    }

    public void setEndDepth(double endDepth) {
        this.endDepth = endDepth;
    }
}
