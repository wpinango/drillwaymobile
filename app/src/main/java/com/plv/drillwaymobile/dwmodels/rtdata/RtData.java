/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rtdata;

import com.google.gson.Gson;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.variable.DataMaster;

import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 *
 * @author JoseAntonio
 */
public class RtData {

    private HashMap<Enum.MNemonic, String> variablesValues;
    private LinkedHashMap<Enum.Source,TramaParsed> tramaParsed;
    private JobStatus jobStatus;
    private String dbaseName;
    private String wellName;
    public double lagStrokeTotal;
    private LinkedHashMap<Enum.MNemonic, DataMaster> dataMaster;
    private String rigName;

    public RtData() {
        variablesValues = new HashMap<>();
        for (Enum.MNemonic value : Enum.MNemonic.values()) {
            variablesValues.put(value, "");
        }
       // tramaParsed = new TramaParsed[com.plogging.dwmodels.Enum.Source.values().length];
       // for (int i = 0; i < tramaParsed.length; i++) {
       //     tramaParsed[i] = new TramaParsed();
       // }
        jobStatus = new JobStatus();
        dataMaster = new LinkedHashMap<>();
    }

    public String getWellName() {
        return wellName;
    }

    public void setWellName(String wellName) {
        this.wellName = wellName;
    }
    

    public String getDbaseName() {
        return dbaseName;
    }

    public void setDbaseName(String dbaseName) {
        this.dbaseName = dbaseName;
    }

    public LinkedHashMap<Enum.MNemonic, DataMaster> getDataMaster() {
        return dataMaster;
    }

    public void setDataMaster(LinkedHashMap<Enum.MNemonic, DataMaster> dataMaster) {
        this.dataMaster = dataMaster;
    }
    
    public DataMaster getDataMaster(Enum.MNemonic nemonic){
        return  this.dataMaster.get(nemonic);
    }

    public JobStatus getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(JobStatus jobStatus) {
        this.jobStatus = jobStatus;
    }

    public HashMap<Enum.MNemonic, String> getVariablesValues() {
        return variablesValues;
    }

    public String getVariableValue(Enum.MNemonic nemonic) {
        return variablesValues.get(nemonic);
    }

    public int getVariableValueInt(Enum.MNemonic nemonic) {
        return (int) Double.parseDouble(variablesValues.get(nemonic).equals("") ?  "0.0":variablesValues.get(nemonic));
    }

    public double getVariableValueDouble(Enum.MNemonic nemonic) {
        return Double.parseDouble(variablesValues.get(nemonic).equals("") ?  "0.0":variablesValues.get(nemonic));
    }

    public long getVariableValueLong(Enum.MNemonic nemonic) {
        return (long) Double.parseDouble(variablesValues.get(nemonic).equals("") ?  "0.0":variablesValues.get(nemonic));
    }

    public void setVariableValue(Enum.MNemonic nemonic, String value) {
        variablesValues.put(nemonic, value);
    }

    public void setVariableValue(Enum.MNemonic nemonic, Double value) {
        variablesValues.put(nemonic, String.valueOf(value));
    }

    public void setVariableValue(Enum.MNemonic nemonic, int value) {
        variablesValues.put(nemonic, String.valueOf(value));
    }

    public void setVariableValue(Enum.MNemonic nemonic, Long value) {
        variablesValues.put(nemonic, String.valueOf(value));
    }

    public void setVariableValue(Enum.MNemonic nemonic, Float value) {
        variablesValues.put(nemonic, String.valueOf(value));
    }

    public void setVariablesValues(HashMap<Enum.MNemonic, String> variablesValues) {
        this.variablesValues = variablesValues;
    }

    public LinkedHashMap<Enum.Source,TramaParsed> getTramaParseds() {
        return tramaParsed;
    }

    public synchronized TramaParsed getTramaParsed(Enum.Source index) {
        return tramaParsed.get(index);
    }

    public void setTramaParsed(LinkedHashMap<Enum.Source,TramaParsed> tramaParsed) {
        this.tramaParsed = tramaParsed;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public void setRigName(String rigName) {
        this.rigName = rigName;
    }

    public String getRigName() {
        return rigName;
    }

}
