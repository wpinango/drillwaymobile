/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.variable;

import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.Unit;

import java.lang.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
public class DataMaster implements Cloneable {

    private Enum.VarType type;
    private Enum.Source source;
    private int channel;
    private boolean changeValue;
    private boolean autoSave;
    private boolean saveOnLag;
    private boolean baseUnitChange;
    private boolean activeForSum;
    private Enum.UnitFamily unitFamily;
    private Unit baseUnit;
    private String[] tagWitsServer;
    private String tagWitsClient;
    private Override override;
    private Calibration calibration;

    @java.lang.Override
    public Object clone() {
        DataMaster obj = null;
        try {
            obj = (DataMaster) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(DataMaster.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (int i = 0; i < obj.tagWitsServer.length; i++) {
            obj.tagWitsServer[i] = new String(obj.tagWitsServer[i]);
        }
        obj.override = (Override) obj.override.clone();
        obj.calibration = (Calibration) obj.calibration.clone();

        return obj;
    }

    public DataMaster() {
        override = new Override();
        calibration = new Calibration();
        //TODO Borrar el siguiente codgo
        type = Enum.VarType.OTHER;
        source = Enum.Source.RTD;
        tagWitsClient = "0000";
        tagWitsServer = new String[4];
        tagWitsServer[0] = "0100";
        tagWitsServer[1] = "0200";
        tagWitsServer[2] = "1100";
        tagWitsServer[3] = "1300";
        baseUnit = new Unit("unit", "unit", 1f);
        unitFamily = Enum.UnitFamily.OTHER;
    }

    public boolean isBaseUnitChange() {
        return baseUnitChange;
    }

    public void setBaseUnitChange(boolean baseUnitChange) {
        this.baseUnitChange = baseUnitChange;
    }

    public Enum.UnitFamily getUnitFamily() {
        return unitFamily;
    }

    public void setUnitFamily(Enum.UnitFamily unitFamily) {
        this.unitFamily = unitFamily;
    }

    public Unit getBaseUnit() {
        return baseUnit;
    }

    public void setBaseUnit(Unit baseUnit) {
        this.baseUnit = baseUnit;
    }

    public Override getOverride() {
        return override;
    }

    public void setOverride(Override override) {
        this.override = override;
    }

    public Calibration getCalibration() {
        return calibration;
    }

    public void setCalibration(Calibration calibration) {
        this.calibration = calibration;
    }

    public boolean isActiveForSum() {
        return activeForSum;
    }

    public void setActiveForSum(boolean activeForSum) {
        this.activeForSum = activeForSum;
    }

    public String[] getTagWitsServer() {
        return tagWitsServer;
    }

    public String getTagWitsServer(int index) {
        return tagWitsServer[index];
    }

    public void setTagWitsServer(String[] tagWitsServer) {
        this.tagWitsServer = tagWitsServer;
    }

    public void setTagWitsServer(String tagWitsServer, int index) {
        this.tagWitsServer[index] = tagWitsServer;
    }

    public String getTagWitsClient() {
        return tagWitsClient;
    }

    public void setTagWitsClient(String tagWitsClient) {
        this.tagWitsClient = tagWitsClient;
    }
 
    public Override.Operator getOWROperator() {
        return override.getOperator();
    }

    public void setOWROperator(Override.Operator Operator) {
        this.override.setOperator(Operator);
    }

    public Override.OWRStatus getOWRStatus() {
        return override.getStatus();
    }

    public void setOWRStatus(Override.OWRStatus OWRStatus) {
        this.override.setStatus(OWRStatus);
    }

    public boolean isCalibration() {
        return calibration.isStatus();
    }

    public void setCalibration(boolean calibration) {
        this.calibration.setStatus(autoSave);
    }

    public boolean isChangeValue() {
        return changeValue;
    }

    public void setChangeValue(boolean change) {
        this.changeValue = change;
    }

    public boolean isAutoSave() {
        return autoSave;
    }

    public void setAutoSave(boolean autoSave) {
        this.autoSave = autoSave;
    }

    public boolean isSaveOnLag() {
        return saveOnLag;
    }

    public void setSaveOnLag(boolean saveOnLag) {
        this.saveOnLag = saveOnLag;
    }

    public Enum.VarType getType() {
        return type;
    }

    public void setType(Enum.VarType type) {
        this.type = type;
    }

    public float getOWRMin() {
        return override.getMin();
    }

    public void setOWRMin(float mOWRMin) {
        this.override.setMin(mOWRMin);
    }

    public float getOWRMax() {
        return override.getMax();
    }

    public void setOWRMax(float mOWRMax) {
        this.override.setMax(mOWRMax);
    }

    public Override.ActiveIf getIsOWRActiveIF() {
        return override.getActiveIF();
    }

    public void setIsOWRActiveIF(Override.ActiveIf mIsOWRActiveIF) {
        this.override.setActiveIF(mIsOWRActiveIF);
    }

    public Enum.MNemonic getOWRVar() {
        return override.getVar();
    }

    public void setOWRVar(Enum.MNemonic VarMNemonic) {
        this.override.setVar(VarMNemonic);
    }

//    public int getOWRCondition() {
//        return override.getCondition();
//    }
//
//    public void setOWRCondition(int mOWRCondition) {
//        this.override.setCondition(mOWRCondition);
//    }
    public float getOWRValue() {
        return override.getValue();
    }

    public void setOWRValue(float mOWRValue) {
        this.override.setValue(mOWRValue);
    }

    public float getOWRFactor() {
        return override.getFactor();
    }

    public void setOWRFactor(float mOWRFactor) {
        this.override.setFactor(mOWRFactor);
    }

    public Enum.Source getSource() {
        return source;
    }

    public void setSource(Enum.Source source) {
        this.source = source;
    }

    public int getChannel() {
        return channel;
    }

    public void setChannel(int mChanel) {
        this.channel = mChanel;
    }

}
