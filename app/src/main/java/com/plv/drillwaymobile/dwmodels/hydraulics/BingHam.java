/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.hydraulics;

/**
 *
 * @author JoseAntonio
 */
public class BingHam {

    class Data {

        public double iner;
        public double anular;

        public Data() {
        }

        @Override
        public String toString() {
            return "Data{" + "iner=" + iner + ", anular=" + anular + '}';
        }

    }

    private double starDepth; //feet
    private double endDept; //feet
    private double pipeId; //in
    private double pipeOd; //in
    private double cassId; //in
    private double mudVP; //yp
    private double mudYP; //cp
    private double mudWH; //lb/gal
    private double cuttSize; //mm
    private double cuttWH; //gr/cm3
    private double gpm;

    ///////////////////////////////////////////////////////////////
    private double anularOD;
    private double seccionLengt;
////////////////////////////////////////////////////////////

    private Data velocity = new Data();
    private Data velocityCrt = new Data();
    private Data pressLoss = new Data();
    private double cuttingVelAnular;
    private Data viscosityAprt = new Data();
    private Data reynolds = new Data();
    private String typeFlow;
    private Data ffMoody = new Data();

    @Override
    public String toString() {
        return "BingHam{" + "pressLoss=" + pressLoss + ", typeFlow=" + typeFlow + '}';
    }

    public BingHam(AnularSection anularSection, double mudVP, double mudYP, double mudWH, double cuttSize, double cuttWH, double gpm) {

        this.starDepth = anularSection.starDepth;
        this.endDept = anularSection.endDept;
        this.pipeId = anularSection.IDpipe;
        this.pipeOd = anularSection.ODpipe;
        this.cassId = anularSection.IDCasing;
        this.mudVP = mudVP;
        this.mudYP = mudYP;
        this.mudWH = mudWH;
        this.cuttSize = cuttSize;
        this.cuttWH = cuttWH;
        this.gpm = gpm;
        ////////////////////////////////////////////
        this.anularOD = cassId - pipeOd;
        this.seccionLengt = this.endDept - this.starDepth;
        this.velocity.iner = getVelocityIner();
        this.velocity.anular = getVelocityAnnular();
        this.velocityCrt.iner = getVelocityCrtIner();
        this.velocityCrt.anular = getVelocityCrtAnular();
        this.viscosityAprt.iner = getViscocityIner();
        this.viscosityAprt.anular = getViscocityAnnular();
        this.reynolds.iner = getReynoldsIner();
        this.reynolds.anular = getReyNoldsAnular();
        this.typeFlow = getTypeFlow();
        this.ffMoody.iner = getffMoody(pipeId, reynolds.iner);
        this.ffMoody.anular = getffMoody(anularOD, reynolds.anular);
        this.pressLoss.iner = getPressLossTurIner();// typeFlow.equals("LAMINAR") ? getPressLossLamIner() : getPressLossTurIner();
        this.pressLoss.anular = typeFlow.equals("LAMINAR") ? getPressLossLamAnular() : getPressLossTurAnular();

    }

    public double getPressDropIn() {
        return pressLoss.iner;
    }

    public double getPressDropOut() {
        return pressLoss.anular;
    }

    private double getVelocityIner() {
        return (double) (gpm / (2.448 * (Math.pow(pipeId, 2))));
    }

    private double getVelocityAnnular() {
        return (double) (gpm / (2.448 * (Math.pow(cassId, 2) - (Math.pow(pipeOd, 2)))));
    }

    private double getViscocityIner() {
        return (double) (((6.66 * mudYP * pipeId) / velocity.iner) + mudVP);
    }

    private double getViscocityAnnular() {
        return (double) (((5 * mudYP * anularOD) / velocity.anular) + mudVP);
    }

    private double getReynoldsIner() {
        return (928 * mudWH * velocity.iner * pipeId) / viscosityAprt.iner;
    }

    private double getReyNoldsAnular() {
        return (928 * mudWH * velocity.anular * anularOD) / viscosityAprt.anular;
    }

    private double getVelocityCrtIner() {
        double cons = 1.078f;
        double cons2 = 12.34f;
        return (double) (((cons * mudVP) + cons * Math.sqrt(Math.pow(mudVP, 2)
                + cons2 * (Math.pow(pipeId, 2)) * mudYP * mudWH)) / (pipeId * mudWH));

    }

    private double getVelocityCrtAnular() {
        double cons = 1.078f;
        double cons2 = 9.256f;
        return (double) (((cons * mudVP) + cons * Math.sqrt(Math.pow(mudVP, 2)
                + cons2 * (Math.pow(anularOD, 2)) * mudYP * mudWH)) / (anularOD * mudWH));
    }

    private String getTypeFlow() {
        return reynolds.anular > 2000f ? "TURBULENTO" : "LAMINAR";
    }

    private double getffMoody(double diameter, double reinold) {
        double fFactor = 0.00001f;
        return (double) (((Math.pow((((fFactor / diameter) * 20000) + (1000000
                / reinold)), (1d / 3d))) + 1) * 0.001375);
    }

    private double getPressLossLamIner() {
        return (double) ((((mudVP * velocity.iner) / (1500 * Math.pow(pipeId, 2)))
                + (mudYP / (225 * pipeId))) * seccionLengt);
    }

    private double getPressLossLamAnular() {
        return (double) ((((mudVP * velocity.anular) / (1000 * Math.pow(anularOD, 2)))
                + (mudYP / (200 * anularOD))) * seccionLengt);
    }

    private double getPressLossTurIner() {
        return (double) ((ffMoody.iner * mudWH * Math.pow(velocity.iner, 2) * seccionLengt)
                / (25.8 * pipeId));
    }

    private double getPressLossTurAnular() {
        return (double) ((ffMoody.anular * mudWH * Math.pow(velocity.anular, 2) * seccionLengt)
                / (21.1 * anularOD));
    }

}
