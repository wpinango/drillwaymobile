/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.wits;

import com.plv.drillwaymobile.dwmodels.Enum;

import java.util.LinkedHashMap;

/**
 *
 * @author JoseAntonio
 */
public class Record {

    private String name;
    private String trigger;
    private String description;
    private String frecuency;
    private long init;
    private long end;
    private boolean header;
    private boolean enable;
    private LinkedHashMap<Enum.MNemonic, WitsVar> WitsVars;

    public Record(String name,String trigger) {
        WitsVars = new LinkedHashMap<>();
          int count = 0;
        for (Enum.MNemonic value : Enum.MNemonic.values()) {
            WitsVar wv = new WitsVar();
            wv.setNemonic(value);
            wv.setActive(false);
            wv.setTag(String.format("%04d", count++));
            WitsVars.put(value,wv);
        }
        this.name = name;
        this.trigger = trigger;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFrecuency() {
        return frecuency;
    }

    public void setFrecuency(String frecuency) {
        this.frecuency = frecuency;
    }

    public LinkedHashMap<Enum.MNemonic, WitsVar> getWitsVars() {
        return WitsVars;
    }

    public void setWitsVars(LinkedHashMap<Enum.MNemonic, WitsVar> WitsVars) {
        this.WitsVars = WitsVars;
    }
    
    public  WitsVar getWitsVar(Enum.MNemonic nemonic) {
        return WitsVars.get(nemonic);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    public long getInit() {
        return init;
    }

    public void setInit(long init) {
        this.init = init;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public boolean isHeader() {
        return header;
    }

    public void setHeader(boolean header) {
        this.header = header;
    }

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }





}
