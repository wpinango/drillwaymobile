/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rigdata;

import com.google.gson.Gson;
import com.plv.drillwaymobile.dwmodels.Enum;

/**
 *
 * @author JoseAntonio
 */
public class DaqSetting {

    private Enum.Protocol protocol;
    private Enum.Source name;
    private String host;
    private int port;
    private Enum.SourceStatus status;
    private int sizeChannelDigital;
    private int sizeChannelAnalog;

    public int getSizeChannelDigital() {
        return sizeChannelDigital;
    }

    public void setSizeChannelDigital(int sizeChannelDigital) {
        this.sizeChannelDigital = sizeChannelDigital;
    }

    public int getSizeChannelAnalog() {
        return sizeChannelAnalog;
    }

    public void setSizeChannelAnalog(int sizeChannelAnalog) {
        this.sizeChannelAnalog = sizeChannelAnalog;
    }

    
    
    public Enum.Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Enum.Protocol protocol) {
        this.protocol = protocol;
    }

    
    
    public Enum.Source getSource() {
        return name;
    }

    public void setName(Enum.Source name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Enum.SourceStatus getStatus() {
        return status;
    }

    public void setStatus(Enum.SourceStatus status) {
        this.status = status;
    }
    
    public String toJson(){
        return new Gson().toJson(this);
    }

}
