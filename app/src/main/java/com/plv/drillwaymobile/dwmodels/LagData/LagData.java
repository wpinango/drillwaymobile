/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.LagData;

import java.util.ArrayList;

/**
 *
 * @author JoseAntonio
 */
public class LagData {

    private ArrayList<Sample> samples;
    //public float lagStrokeTotal;

    public LagData() {
        samples = new ArrayList<>();
    }

    public ArrayList<Sample> getSamples() {
        return samples;
    }

    public void setSamples(ArrayList<Sample> samples) {
        this.samples = samples;
    }

    public Sample getSample(int index) {
        return samples.get(index);
    }

    public void add(Sample sample) {
        this.samples.add(sample);
    }

    public void add(int index, Sample sample) {
        this.samples.add(index, sample);
    }

    public void remove(Sample sample) {
        this.samples.remove(sample);
    }
    public void remove(int index) {
        this.samples.remove(index);
    }

    public void set(int index, Sample sample) {
        this.samples.set(index, sample);
    }

    public int size() {
        return this.samples.size();
    }
}
