package com.plv.drillwaymobile.dwmodels.rigdata;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.hydraulics.Rheology;
import com.plv.drillwaymobile.dwmodels.variable.DataMaster;
import com.plv.drillwaymobile.dwmodels.wits.WitsConfig;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.plv.drillwaymobile.dwmodels.Constant.FILE_RIGDATA;
import static com.plv.drillwaymobile.dwmodels.Constant.PATH_SETTING;
import static com.plv.drillwaymobile.dwmodels.Util.writeFileJSON;

/**
 *
 * @author JoseAntonio
 */
public class RigData implements Cloneable {

    private double standAverage;
    private double pipeAverage;
    private String rigName;
    private int startDepth;
    private double minHokkLoad;
    private double minPumpPress;
    private double factMotor;
    private double lastSurvey;
    private double angle;
    private double lastTVD;
    private double bhaLeng;
    private LinkedHashMap<Enum.Source,DaqSetting> daqSettings;
    private ArrayList<Pump> pumps;
    private ArrayList<Bha> bhas;
    private ArrayList<Casing> casings;
    private SetPointActivity setPointActivity;
    private Encoder encoder;
    private Rheology rheology;
    private ArrayList<WitsConfig> witsConfigs;
    private LinkedHashMap<Enum.MNemonic, DataMaster> variablesDataMaster;

    
    
    @Override
    public Object clone() {
        RigData obj = null;
        try {
            obj = (RigData) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(RigData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public LinkedHashMap<Enum.MNemonic, DataMaster> getVariablesDataMaster() {
        return variablesDataMaster;
    }

    public DataMaster getVariableDataMaster(Enum.MNemonic mNemonic) {
        return variablesDataMaster.get(mNemonic);
    }

    public void setVariablesDataMaster(LinkedHashMap<Enum.MNemonic, DataMaster> variablesDataMaster) {
        this.variablesDataMaster = variablesDataMaster;
    }
    public void putVariableDataMaster(Enum.MNemonic mn, DataMaster dm) {
        this.variablesDataMaster.put(mn, dm);
    }

    public double getStandAverage() {
        return standAverage;
    }

    public void setStandAverage(double standAverage) {
        this.standAverage = standAverage;
    }

    public double getPipeAverage() {
        return pipeAverage;
    }

    public void setPipeAverage(double pipeAverage) {
        this.pipeAverage = pipeAverage;
    }

    public double getBhaLeng() {
        return bhaLeng;
    }

    public void setBhaLeng(double bhaLeng) {
        this.bhaLeng = bhaLeng;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public RigData() {
      //  weelName = "well";
        //  rigName = "rig";
//        pumps = new ArrayList<>();
//        for (int i = 0; i < 6; i++) {
//            pumps.add(new Pump());
//        }
//        
//        bhas = new ArrayList<>();
//        for (int i = 0; i < 8; i++) {
//            bhas.add(new Bha());
//        }
//        
//        cassings = new ArrayList<>();
//        for (int i = 0; i < 4; i++) {
//            cassings.add(new Cassing());
//        }

    }

    public ArrayList<WitsConfig> getWitsConfigs() {
        return witsConfigs;
    }

    public void setWitsConfigs(ArrayList<WitsConfig> witsConfigs) {
        this.witsConfigs = witsConfigs;
    }
    
    public Rheology getRheology() {
        return rheology;
    }
    

    public void setRheology(Rheology rheology) {
        this.rheology = rheology;
    }

    public double getLastSurvey() {
        return lastSurvey;
    }

    public void setLastSurvey(double lastSurvey) {
        this.lastSurvey = lastSurvey;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double getLastTVD() {
        return lastTVD;
    }

    public void setLastTVD(float lastTVD) {
        this.lastTVD = lastTVD;
    }

    public Encoder getEncoder() {
        return encoder;
    }

    public void setEncoder(Encoder encoder) {
        this.encoder = encoder;
    }

    public SetPointActivity getSetPointActivity() {
        return setPointActivity;
    }

    public void setSetPointActivity(SetPointActivity setPointActivity) {
        this.setPointActivity = setPointActivity;
    }

    public double getFactMotor() {
        return factMotor;
    }

    public void setFactMotor(double factMotor) {
        this.factMotor = factMotor;
    }
    public void save() {
        writeFileJSON(this, PATH_SETTING, FILE_RIGDATA);
    }
    public double getMinHokkLoad() {
        return minHokkLoad;
    }

    public void setMinHokkLoad(double minHokkLoad) {
        this.minHokkLoad = minHokkLoad;
    }

    public double getMinPumpPress() {
        return minPumpPress;
    }

    public void setMinPumpPress(float minPumpPress) {
        this.minPumpPress = minPumpPress;
    }

    public LinkedHashMap<Enum.Source,DaqSetting> getDaqSettings() {
        return daqSettings;
    }
    public DaqSetting getDaqSetting(Enum.Source source) {
        return daqSettings.get(source);
    }
    public void setDaqSettings(LinkedHashMap<Enum.Source,DaqSetting> daqSettings) {
        this.daqSettings = daqSettings;
    }

//    public String getWeelName() {
//        return weelName;
//    }
//
//    public void setWeelName(String weelName) {
//        this.weelName = weelName;
//    }

    public String getRigName() {
        return rigName;
    }

    public void setRigName(String rigName) {
        this.rigName = rigName;
    }

    public int getStartDepth() {
        return startDepth;
    }

    public void setStartDepth(int startDepth) {
        this.startDepth = startDepth;
    }

    public ArrayList<Pump> getPumps() {
        return pumps;
    }

    public Pump getPump(int index) {
        return pumps.get(index);
    }

    public void setPumps(ArrayList<Pump> pumps) {
        this.pumps = pumps;
    }

    public void setPump(Pump pump, int index) {
        this.pumps.set(index, pump);
    }

    public ArrayList<Bha> getBhas() {
        return bhas;
    }

    public void setBhas(ArrayList<Bha> bhas) {
        this.bhas = bhas;
    }

    public ArrayList<Casing> getCasings() {
        return casings;
    }

    public void setCasings(ArrayList<Casing> cassings) {
        this.casings = cassings;
    }

    public String getDaqHost(Enum.Source index) {
        return daqSettings.get(index).getHost();
    }

    public void setDaqHost(String host, Enum.Source index) {
        this.daqSettings.get(index).setHost(host);
    }

    public int getDaqPort(Enum.Source index) {
        return daqSettings.get(index).getPort();
    }

    public void setDaqPort(int port, Enum.Source index) {
        this.daqSettings.get(index).setPort(port);
    }

    public Enum.SourceStatus getDaqStatus(Enum.Source index) {
        return daqSettings.get(index).getStatus();
    }

    public void setDaqStatus(Enum.SourceStatus status, Enum.Source index) {
        this.daqSettings.get(index).setStatus(status);
    }

//    public String getDBaseName() {
//        return this.weelName.equals("") ? this.rigName : (this.rigName + "_" + this.weelName);
//    }
//
//    public void setDBname(String jobName) {
//        String[] ses = jobName.split("_");
//        if (ses.length > 1) {
//            this.rigName = ses[0];
//            this.weelName = ses[1];
//        } else {
//            this.rigName = jobName;
//            this.weelName = "";
//        }
//    }
    
    public String toJson(){
        return new Gson().toJson(this);
    }
}
