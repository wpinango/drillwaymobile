/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.variable;

import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.Unit;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
public class DataLocal implements Cloneable {

    private String display;
    private String description;
    private int format;
    private boolean alarm;
    private float alarmMin;
    private float alarmMax;
    private float trendMin = 0;
    private float trendMax = 5000;
    private Unit displayUnit;
    private boolean viewChart;
    private Enum.TypeDataBox typeDataBox;

    public Enum.TypeDataBox getTypeDataBox() {
        return typeDataBox;
    }

    public void setTypeDataBox(Enum.TypeDataBox typeDataBox) {
        this.typeDataBox = typeDataBox;
    }

   public boolean isViewChart() {
        return viewChart;
    }

    public void setViewChart(boolean viewChart) {
        this.viewChart = viewChart;
    }

    @java.lang.Override
    public Object clone() {
        DataLocal obj = null;
        try {
            obj = (DataLocal) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(DataLocal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    public float getTrendMin() {
        return trendMin;
    }

    public void setTrendMin(float trendMin) {
        this.trendMin = trendMin;
    }

    public float getTrendMax() {
        return trendMax;
    }

    public void setTrendMax(float trendMax) {
        this.trendMax = trendMax;
    }

    public DataLocal(String displa) {
        displayUnit = new Unit("unit", "unit", 1f);
        display = displa;
        description = displa;
    }

    public Unit getDisplayUnit() {
        return displayUnit;
    }

    public void setDisplayUnit(Unit displayUnit) {
        this.displayUnit = displayUnit;
    }

    public boolean isAlarm() {
        return alarm;
    }

    public void setAlarm(boolean alarm) {
        this.alarm = alarm;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String mDisplay) {
        this.display = mDisplay;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String mDescription) {
        this.description = mDescription;
    }

    public int getFormat() {
        return format;
    }

    public void setFormat(int mFormat) {
        this.format = mFormat;
    }

    public float getAlarmMin() {
        return alarmMin;
    }

    public void setAlarmMin(float mAlarmMin) {
        this.alarmMin = mAlarmMin;
    }

    public float getAlarmMax() {
        return alarmMax;
    }

    public void setAlarmMax(float mAlarmMax) {
        this.alarmMax = mAlarmMax;
    }



}
