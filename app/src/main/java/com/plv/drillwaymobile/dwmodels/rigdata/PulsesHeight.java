/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rigdata;

import com.google.gson.Gson;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
public class PulsesHeight implements Cloneable {

    public String[] pulses = new String[5];
    public String[] height = new String[5];

    @Override
    public Object clone() throws CloneNotSupportedException {
        PulsesHeight obj = (PulsesHeight) super.clone();
        return obj;
    }

    /**
     *
     * @param index el indice de la altura deseada
     * @return Retorna altura en float
     */
    public float getHeightFloat(int index) {
        return Float.valueOf(height[index]);
    }

    /**
     *
     * @return Retorna arreglo en float de todas las alturas
     */
    public float[] getHeightsFloat() {
        float[] tempfloat = new float[height.length];
        for (int i = 0; i < height.length; i++) {
            tempfloat[i] = Float.valueOf(height[i]);
        }
        return tempfloat;
    }

    /**
     *
     * @param index el indice del pulso deseada
     * @return Retorna el pulso en int
     */
    public int getPulseInt(int index) {
        return Integer.valueOf(pulses[index]);
    }

    /**
     *
     * @return Retorna arreglo de int de todas los pulsos
     */
    public int[] getPulsesInt() {
        int[] tempint = new int[pulses.length];
        for (int i = 0; i < pulses.length; i++) {
            tempint[i] = Integer.valueOf(pulses[i]);
        }
        return tempint;
    }

    /**
     *
     * @return Retorna un string con todos los pulsos separados por "|"
     */
    public String getPulses() {
        return String.join(",", pulses);
    }

    /**
     *
     * @return Retorna un string con todas las alturas separados por "|"
     */
    public String getHeigths() {
        return String.join(",", height);
    }

    /**
     * Transfoma y sete el valor del pulso.
     *
     * @param i
     * @param pulse
     */
    public void setPulse(int i, int pulse) {
        pulses[i] = String.valueOf(pulse);
    }

    /**
     * Transfoma y sete el valor de la altura.
     *
     * @param i
     * @param heigth
     */
    public void setHeigth(int i, float heigth) {
        height[i] = String.valueOf(heigth);
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

}
