/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels;

import com.google.gson.Gson;

/**
 *
 * @author JoseAntonio
 */
public class Comment {
    
    public long id;
    public String Comment;
    public Enum.MNemonic nemonic;

    public String toJson() {
        return new Gson().toJson(this);
        }
    
}
