/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.hydraulics;

import com.plv.drillwaymobile.dwmodels.rigdata.RigData;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
public class Rheology implements Cloneable {

    public final int f3 = 0;
    public final int f6 = 1;
    public final int f100 = 2;
    public final int f200 = 3;
    public final int f300 = 4;
    public final int f600 = 5;
    public final int[] f = {3, 6, 100, 200, 300, 600};
    /////////////////////////////
  @Override
    public Object clone() {
        Rheology obj = null;
        try {
            obj = (Rheology) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(RigData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
    public ArrayList<Float> fanns = new ArrayList<>();
        public float mudWh;
    public float cuttD;
    public float cuttWh;
    /////////////////F//////////////
    public ArrayList<Float> ShearRate = new ArrayList<>();
    public ArrayList<Float> ShearStressT = new ArrayList<>();
    public ArrayList<Float> AparentVisco = new ArrayList<>();
    public ArrayList<Float> TrueYield = new ArrayList<>();
    public ArrayList<Float> ShearStress = new ArrayList<>();
    //////////////////////////////////
    public float PLn;
    public float PLk;
    public float HBn;
    public float HBk;
    public float yp;
    public float pv;
    public float gel;


    public Rheology(float f3, float f6, float f100, float f200, float f300, float f600,float mudWh,float cuttWH,float cuttD) {

        this.fanns.add(f3);
        this.fanns.add(f6);
        this.fanns.add(f100);
        this.fanns.add(f200);
        this.fanns.add(f300);
        this.fanns.add(f600);
        this.mudWh = mudWh;
        this.cuttD = cuttD;
        this.cuttWh = cuttWH;
        prevCalc();
        calcPL();
        calcHB();
        calcBing();

    }

    @Override
    public String toString() {
        return "MudRheology{" + "PLn=" + PLn + ", PLk=" + PLk + ", HBn=" + HBn + ", HBk=" + HBk + ", yp=" + yp + ", pv=" + pv + '}';
    }

    private void prevCalc() {
        for (int i = 0; i < fanns.size(); i++) {
            ShearRate.add(f[i] * 1.7032f);
            ShearStressT.add(fanns.get(i) * 5.1f);
            AparentVisco.add(ShearStressT.get(i)*100/ShearRate.get(i));
            TrueYield.add((2*fanns.get(f3)-fanns.get(f6))*4.79f);
            ShearStress.add(ShearStressT.get(i)-TrueYield.get(i));  
        }

    }

    private void calcPL() {
        PLn = (float) ((0.5f)*(Math.log10(ShearStressT.get(f300)/ShearStressT.get(f3))));
        PLk = (float) ((ShearStressT.get(f300)*100)/(Math.pow(ShearRate.get(f300), PLn)));
    }
    private void calcHB() {
        HBn = (float) ((0.5f)*(Math.log10(ShearStress.get(f300)/ShearStress.get(f3))));
        HBk = (float) ((ShearStress.get(f300)*100)/(Math.pow(ShearRate.get(f300), HBn))); 
    }

    private void calcBing() {
        pv = fanns.get(f600)-fanns.get(f300);
        yp = fanns.get(f300)-pv;
        gel = fanns.get(f3);
    }

}
