/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.hydraulics;

/**
 *
 * @author JoseAntonio
 */
public class AnularSection {

    class Data {

        public double iner;
        public double anular;

        public Data() {
        }

        @Override
        public String toString() {
            return "Data{" + "iner=" + iner + ", anular=" + anular + '}';
        }

    }
    public double starDepth;
    public double endDept;
    public double IDpipe;
    public double IDCasing;
    public double ODpipe;
    public double lengt;
    private Data volumen = new Data();

    public AnularSection(double starDepth, double endDept, double IDpipe, double IDCasing, double ODpipe) {
        this.starDepth = starDepth;
        this.endDept = endDept;
        this.IDpipe = IDpipe;
        this.IDCasing = IDCasing;
        this.ODpipe = ODpipe;
        this.lengt = endDept - starDepth;
        this.volumen.anular = getVolumeAnular();
        this.volumen.iner = getVolumeIner();
    }

    public double getVolumeAnu() {
        return this.volumen.anular;
    }

    public double getVolumeIn() {
        return this.volumen.iner;
    }

    private double getVolumeIner() {
        return (double) (((Math.pow(IDpipe, 2)) / 1029.49) * lengt);
    }

    private double getVolumeAnular() {
        return (double) ((((Math.pow(IDCasing, 2)) - (Math.pow(ODpipe, 2))) / 1029.49) * lengt);  
    }

    public double getIDCasing() {
        return IDCasing;
    }

    @Override
    public String toString() {
        return "AnularSection{" + "starDepth=" + starDepth + ", endDept=" + endDept + ", IDpipe=" + IDpipe + ", IDCasing=" + IDCasing + ", ODpipe=" + ODpipe + ", lengt=" + lengt + ", volumen=" + volumen + '}';
    }

}
