/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.hydraulics;

/**
 *
 * @author JoseAntonio
 */
public class HersBulk {

    class Data {

        public double iner;
        public double anular;

        public Data() {
        }

        @Override
        public String toString() {
            return "Data{" + "iner=" + iner + ", anular=" + anular + '}';
        }

    }

    private double starDepth; //feet
    private double endDept; //feet
    private double pipeId; //in
    private double pipeOd; //in
    private double cassId; //in
    private double n; //yp
    private double K; //cp
    private double mudWH; //lb/gal
    private double cuttSize; //mm
    private double cuttWH; //gr/cm3
    private double gpm;
    ///////////////////////////////////////////////////////////////
    private double anularOD;
    private double seccionLengt;
    /////////////////////////////////////////////////////////
    private Data velocity = new Data();
    private Data pressLoss = new Data();
    private double cuttingVelAnular;
    private Data reynolds = new Data();
    private String typeFlow;
    private Data ffMoody = new Data();

    public HersBulk(AnularSection anularSection, double n, double K, double mudWH, double cuttSize, double cuttWH, double gpm) {
        this.starDepth = anularSection.starDepth;
        this.endDept = anularSection.endDept;
        this.pipeId = anularSection.IDpipe;
        this.pipeOd = anularSection.ODpipe;
        this.cassId = anularSection.IDCasing;
        this.n = n;
        this.K = K;
        this.mudWH = mudWH;
        this.cuttSize = cuttSize;
        this.cuttWH = cuttWH;
        this.gpm = gpm;
        ////////////////////////////////////////////
        this.anularOD = cassId - pipeOd;
        this.seccionLengt = this.endDept - this.starDepth;
        this.velocity.iner = getVelocityIner();
        this.velocity.anular = getVelocityAnnular();
        this.reynolds.iner = getReynoldsIner();
        this.reynolds.anular = getReyNoldsAnular();
        this.typeFlow = getTypeFlow();
        this.ffMoody.iner = getffMoody(pipeId, reynolds.iner);
        this.ffMoody.anular = getffMoody(anularOD, reynolds.anular);
        this.pressLoss.iner = getPressLossTurIner();// typeFlow.equals("LAMINAR") ? getPressLossLamIner() : getPressLossTurIner();
        this.pressLoss.anular = typeFlow.equals("LAMINAR") ? getPressLossLamAnular() : getPressLossTurAnular();
    }

    @Override
    public String toString() {
        return "HersBulk{" + "pressLoss=" + pressLoss + ", typeFlow=" + typeFlow + '}';
    }

    public double getPressDropIn() {
        return pressLoss.iner;
    }

    public double getPressDropOut() {
        return pressLoss.anular;
    }

    private double getVelocityIner() {
        return (double) (gpm / (2.448 * (Math.pow(pipeId, 2))));
    }

    private double getVelocityAnnular() {
        return (double) (gpm / (2.448 * (Math.pow(cassId, 2) - (Math.pow(pipeOd, 2)))));
    }

    private double getReynoldsIner() {
        return (double) (((89100 * mudWH * Math.pow(velocity.iner, 2 - n)) / K) * (Math.pow((0.0416f * pipeId) / (3 + 1 / n), n)));
    }

    private double getReyNoldsAnular() {
        return (double) (((109000 * mudWH * Math.pow(velocity.anular, 2 - n)) / K) * (Math.pow((0.0208f * anularOD) / (2 + 1 / n), n)));
    }

    private String getTypeFlow() {
        return reynolds.anular > 3000f ? "TURBULENTO" : "LAMINAR";
    }

    private double getffMoody(double diameter, double reinold) {
        double fFactor = 0.00001f;
        return (double) (((Math.pow((((fFactor / diameter) * 20000) + (1000000
                / reinold)), (1d / 3d))) + 1) * 0.001375);
    }

    private double getPressLossLamIner() {
        return (double) (((seccionLengt * K * Math.pow(velocity.iner, n))
                / (144000 * Math.pow(pipeId, 1 + n)))
                * (Math.pow((3 + 1 / n) / 0.0416, n)));
    }

    private double getPressLossLamAnular() {
        return (double) (((seccionLengt * K * Math.pow(velocity.anular, n))
                / (144000 * Math.pow(anularOD, 1 + n)))
                * (Math.pow((2 + 1 / n) / 0.0208, n)));
    }

    private double getPressLossTurIner() {
        return (double) ((ffMoody.iner * mudWH * Math.pow(velocity.iner, 2) * seccionLengt)
                / (25.8 * pipeId));
    }

    private double getPressLossTurAnular() {
        return (double) ((ffMoody.anular * mudWH * Math.pow(velocity.anular, 2) * seccionLengt)
                / (21.1 * anularOD));
    }

}
