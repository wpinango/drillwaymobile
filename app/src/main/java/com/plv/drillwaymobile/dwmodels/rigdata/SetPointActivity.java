/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rigdata;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
public class SetPointActivity implements Cloneable {
    private boolean active;
    private float pumpPressure;
    private float SPMTotal;
    private float RPMTotal;
    private float torque;
    private float diffDepth;
    private float diffReam;

     @Override
    public Object clone() {
        SetPointActivity obj = null;
        try {
            obj = (SetPointActivity) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(RigData.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }
    
    
    
    
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean status) {
        this.active = status;
    }

    
    public float getPumpPressure() {
        return pumpPressure;
    }

    public void setPumpPressure(float pumpPressure) {
        this.pumpPressure = pumpPressure;
    }

    public float getSPMTotal() {
        return SPMTotal;
    }

    public void setSPMTotal(float SPMTotal) {
        this.SPMTotal = SPMTotal;
    }

    public float getRPMTotal() {
        return RPMTotal;
    }

    public void setRPMTotal(float RPMTotal) {
        this.RPMTotal = RPMTotal;
    }

    public float getTorque() {
        return torque;
    }

    public void setTorque(float torque) {
        this.torque = torque;
    }

    public float getDiffDepth() {
        return diffDepth;
    }

    public void setDiffDepth(float diffDepth) {
        this.diffDepth = diffDepth;
    }

    public float getDiffReam() {
        return diffReam;
    }

    public void setDiffReam(float diffReam) {
        this.diffReam = diffReam;
    }
    
    
    
    
    
}
