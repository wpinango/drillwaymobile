/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rtdata;

/**
 *
 * @author JoseAntonio
 */
public class TramaParsed {

    public String trama;
    private String[] data;

    public TramaParsed() {
       
    }

    public String getTrama() {
        return trama;
    }

    public void setTrama(String trama) {
        this.trama = trama;
    }

    public String[] getDatas() {
        return data;
    }

    public synchronized String getData(int index) {
        if(data == null){
            return null;
        }else if(data.length > index ){
            return  data[index];
        }else{
            return null;
        }
        
    }

    public void setDatas(String[] data) {
        this.data = data;
    }

    public Double getDataDouble(int index) {
        try {
            return Double.parseDouble(data[index]);
        } catch (NumberFormatException ex) {
            System.out.println(ex.getMessage());
            return -999999999.0;
        }
    }

}
