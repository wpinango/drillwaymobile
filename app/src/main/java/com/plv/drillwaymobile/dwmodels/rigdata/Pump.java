/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rigdata;

/**
 *
 * @author JoseAntonio
 */
public class Pump {

    private float length;// = 0;
    private float diameter;// = 0;
    private float efficient;// = 0;
    private float bbl_strk;
    private float gal_strk;

    public float getBbl_strk() {
        return bbl_strk;
    }

    public float getGal_strk() {
        return gal_strk;
    }
    
    public float getLength() {
        return length;
    }

    public void setLength(float length) {
        this.length = length;
        calcFactors();
    }

    public float getDiameter() {
        return diameter;
    }

    public void setDiameter(float diameter) {
        this.diameter = diameter;
        calcFactors();
    }

    public float getEfficient() {
        return efficient;
    }

    public void setEfficient(float efficient) {
        this.efficient = efficient;
        calcFactors();
    }
    
    private void calcFactors(){
        
        bbl_strk = (((float) Math.pow(diameter, 2)) * length * 0.000243f) * (efficient / 100);
        gal_strk =  bbl_strk * 42;
    }

}
