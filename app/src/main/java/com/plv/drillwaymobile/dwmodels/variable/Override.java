/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.variable;

import com.plv.drillwaymobile.dwmodels.Enum;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author JoseAntonio
 */
public class Override implements Cloneable{

    private OWRStatus Status;
    private float min;
    private float max;
    private ActiveIf activeIf;
    private Enum.MNemonic variable;
    //private int condition;
    private float value;
    private float factor;
    private Operator operator;

    
    @java.lang.Override
    protected Object clone(){
        Override obj = null;
        try {
            obj =  (Override) super.clone(); //To change body of generated methods, choose Tools | Templates.
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Override.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return obj;
    }

    
    
    
    public Override() {
        //TODO borrar este codido
        Status = OWRStatus.NOT_USE;
        activeIf = ActiveIf.RAMDOM;
        variable = Enum.MNemonic.SPARE40;
        operator =  operator.different;
        ///////////////////////////////////
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator Operator) {
        this.operator = Operator;
    }

    public OWRStatus getStatus() {
        return Status;
    }

    public void setStatus(OWRStatus Status) {
        this.Status = Status;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float Min) {
        this.min = Min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float Max) {
        this.max = Max;
    }

    public ActiveIf getActiveIF() {
        return activeIf;
    }

    public void setActiveIF(ActiveIf ActiveIF) {
        this.activeIf = ActiveIF;
    }

    public Enum.MNemonic getVar() {
        return variable;
    }

    public void setVar(Enum.MNemonic VarMNemonic) {
        this.variable = variable;
    }

//    public int getCondition() {
//        return condition;
//    }
//
//    public void setCondition(int Condition) {
//        this.condition = Condition;
//    }

    public float getValue() {
        return value;
    }

    public void setValue(float Value) {
        this.value = Value;
    }

    public float getFactor() {
        return factor;
    }

    public void setFactor(float Factor) {
        this.factor = Factor;
    }

    public enum OWRStatus {

        NOT_USE, DISABLE, ENABLE
    }

    public enum ActiveIf {

        RAMDOM,
        VARACTIVE,
        VARDEPEND,
    }

    public enum Operator {

        greater, minor, equal, different;
        
        @java.lang.Override
        public String toString() {
            switch (this) {
                case different:
                    return "!=";
                case equal:
                    return "==";
                case greater:
                    return ">";
                case minor:
                    return "<";
            }
            return null;
        }

    }
}
