/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels;

import android.widget.TextView;

import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
/*import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;*/

/**
 *
 * @author JoseAntonio
 */
public class Util {

    /**
     *
     * @param path
     * @param extension
     * @return
     */
    /*public static ArrayList<File> getFileList(String path, String extension) {
        try {
            ArrayList<File> files = new ArrayList<>();
            String basePath = new File(".").getCanonicalPath();
            String settingsPath = basePath +"/"+ path;
            File folder = new File(settingsPath);
            File[] listOfFiles = folder.listFiles();
            if (listOfFiles != null) {
                for (File listOfFile : listOfFiles) {
                    if (listOfFile.isFile()
                            && FilenameUtils.getExtension(listOfFile.getName()).equals(extension)) {
                        files.add(listOfFile);
                    }
                }
            }
            return files.size() > 0 ? files : null;
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (Exception ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    *//**
     * Metodo para .
     *
     * @param pathFile directorio
     * @param file nombre del archivo con extencion
     * @return String estructura Json
     *//*
    public static FileHandler getFileHandler() {
        try {
            String settingsPathFile = "";
            String path = "/logs/";
            String basePath = new File(".").getCanonicalPath();
            settingsPathFile = basePath + path + "error";
            File dirSetting = new File(basePath + path);
            if (!dirSetting.exists()) {
                dirSetting.mkdirs();
            }
            SimpleFormatter formatter = new SimpleFormatter();
            FileHandler fh = new FileHandler(settingsPathFile, 100000, 100, true);
            fh.setFormatter(formatter);
            return fh;
        } catch (IOException | SecurityException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    *//**
     * Metodo para leer la ubicacion de un archivo de Texto.
     *
     * @param pathFile directorio
     * @param file nombre del archivo con extencion
     * @return String estructura Json
     *//*
    public static String readFile(String pathFile, String file) {
        String settingsPathFile = "";
        try {
            String basePath = new File(".").getCanonicalPath();
            settingsPathFile = basePath + "/" + pathFile + "/" + file;
            return new String(Files.readAllBytes(Paths.get(settingsPathFile)));
        } catch (IOException ex) {
            System.out.println("Error al leer File " + settingsPathFile);
            return null;
        }
    }
    
        *//**
     * Metodo para leer la ubicacion de un archivo de Texto.
     *
     * @param pathFile directorio
     * @param file nombre del archivo con extencion
     * @return String estructura Json
     *//*
    public static String readFile(File file) {
        try {
            return new String(Files.readAllBytes(Paths.get(file.toURI())));
        } catch (IOException ex) {
            System.out.println("Error al leer File " + file.getAbsolutePath());
            return null;
        }
    }
    
    

    *//**
     * Metodo para leer la ubicacion de un archivo de Texto.
     *
     * @param parent
     * @param file nombre del archivo con extencion
     * @return String estructura Json
     *//*
    public static String readResourceFile(String file, Object parent) {
        try {

            return IOUtils.toString(parent.getClass().getClassLoader().getResourceAsStream("json/" + file));

        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Error al leer File " + file);
            return null;
        }
    }
    
    *//**
     * Metodo para escribir en un archivo TXT una estructura JSON
     *
     * @param json estructura JSON
     * @param pathFile dsubdirectorio
     * @param file nombre del archivo
     *//*
    public static void writeFileJSON(Object json, String pathFile, String file) {
        String settingsPathFile = "";
        try {
            String basePath = new File(".").getCanonicalPath();
            settingsPathFile = basePath + "/" + pathFile + "/" + file;
            File dirSetting = new File(basePath + "/" + pathFile);
            if (!dirSetting.exists()) {
                dirSetting.mkdirs();
            }
            FileWriter fileWriter = new FileWriter(settingsPathFile);
            fileWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(json));
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException ex) {
            System.out.println("Error al Escribir File " + settingsPathFile);
        }
    }

    *//**
     * Metodo para eliminar en un archivo
     *
     * @param pathFile dsubdirectorio
     * @param file nombre del archivo
     * @return true si se elimino o false si no.
     *//*
    public static boolean removeFile(String pathFile, String file) {
        String settingsPathFile = "";
        try {
            String basePath = new File(".").getCanonicalPath();
            settingsPathFile = basePath + "/" + pathFile + "/" + file;
            File fileWriter = new File(settingsPathFile);
            return fileWriter.delete();
        } catch (IOException ex) {
            System.out.println("Error al Escribir File " + settingsPathFile);
            return false;
        }
    }

    *//**
     * Metodo para escribir en un archivo TXT una estructura JSON
     *
     * @param json estructura JSON
     * @param pathFile dsubdirectorio
     *//*
    public static void writeFileJSON(String json, String pathFile) {

        try {
            try (FileWriter fileWriter = new FileWriter(pathFile)) {
                fileWriter.write(json);
                fileWriter.flush();
            }
        } catch (IOException ex) {
            System.out.println("Error al Escribir File " + pathFile);
        }
    }

    *//**
     * Metodo para borrar un archivo.
     *
     * @param pathFile dsubdirectorio
     * @param fileName nombre del archivo
     * @return Verdadero o falso si se pudo borrar
     *//*
    public static boolean delFile(String pathFile, String fileName) {
        String settingsPathFile = "";
        try {
            String basePath = new File(".").getCanonicalPath();
            settingsPathFile = basePath + "/" + pathFile + "/" + fileName;
            File file = new File(settingsPathFile);
            return file.delete();
        } catch (IOException ex) {
            System.out.println("Error al Escribir File " + settingsPathFile);
            return false;
        }
    }

//    public static void writeResourceFile(Object json, String file, Object parent) {
//        try {
//            FileWriter fileWriter = new FileWriter(new File(parent.getClass().getResource("/" + file).toURI()));
//            fileWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(json));
//            fileWriter.flush();
//            fileWriter.close();
//        } catch (IOException | URISyntaxException ex) {
//            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
//            System.out.println("Error al Escribir File " + file);
//        }
//    }
    *//**
     * Metodo que devuelve el tamano optima de una fuente dado su longitud y
     * espacio
     *
     * @param label JLabel donde se encuentra el texto a medir
     * @return
     *//*
    public static int getIntFontSizeLabel(JLabel label) {
        double stringWidth = label.getFontMetrics(label.getFont()).stringWidth(label.getText());

        double componentWidth = 0.8 * label.getWidth();

        // Find out how much the font can grow in width.
        double widthRatio = componentWidth / stringWidth;

        int newFontSize = (int) (label.getFont().getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        return fontSizeToUse;
    }

    *//**
     * Metodo que devuelve el tamano optimo de una fuente dado su longitud y
     * espacio
     *
     * @param label jTextField donde se encuentra el texto a medir
     * @return
     *//*
    public static int getIntFontSizeLabel(JTextField label) {
        double stringWidth = label.getFontMetrics(label.getFont()).stringWidth(label.getText());

        double componentWidth = 0.8 * label.getWidth();

        // Find out how much the font can grow in width.
        double widthRatio = componentWidth / stringWidth;

        int newFontSize = (int) (label.getFont().getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        return fontSizeToUse;
    }

    *//**
     * Metodo que devuelve el tamano optimo de una fuente dado su longitud y
     * espacio
     *
     * @param label jTextField donde se encuentra el texto a medir
     * @return
     *//*
    public static int getIntFontSizeLabel(TextView label) {
        double stringWidth = label.getFontMetrics(label.getFont()).stringWidth(label.getText());

        double componentWidth = 0.8 * label.getWidth();

        // Find out how much the font can grow in width.
        double widthRatio = componentWidth / stringWidth;

        int newFontSize = (int) (label.getFont().getSize() * widthRatio);
        int componentHeight = label.getHeight();

        // Pick a new font size so it will not be larger than the height of label.
        int fontSizeToUse = Math.min(newFontSize, componentHeight);

        // Set the label's font size to the newly determined size.
        return fontSizeToUse;
    }

    *//**
     * Metodo que devuelve el numero romano de un entero
     *
     * @param num entero a transformar
     * @return Texto del numeor romano
     *//*
    public static String intToRoman(int num) {
        StringBuilder sb = new StringBuilder();
        int times = 0;
        String[] romans = new String[]{"I", "IV", "V", "IX", "X", "XL", "L",
            "XC", "C", "CD", "D", "CM", "M"};
        int[] ints = new int[]{1, 4, 5, 9, 10, 40, 50, 90, 100, 400, 500,
            900, 1000};
        for (int i = ints.length - 1; i >= 0; i--) {
            times = num / ints[i];
            num %= ints[i];
            while (times > 0) {
                sb.append(romans[i]);
                times--;
            }
        }
        return sb.toString();
    }

    *//**
     * Metodo para centrar un frame que es llamado por otro, ejecuta setVisible.
     *
     * @param parent Frame padre
     * @param child Frame Hijo
     *//*
    *//*public static void center(JFrame parent, JFrame child) {
        child.setLocation(((parent.getBounds().width - child.getBounds().width) / 2) + parent.getBounds().x,
                ((parent.getBounds().height - child.getBounds().height) / 2) + parent.getBounds().y);
        child.setVisible(true);
    }*//*

    *//**
     * Metodo para centrar un dialog que es llamado por frame, ejecuta
     * setVisible.
     *
     * @param parent Frame padre
     * @param child Frame Hijo
     *//*
    *//*public static void center(JFrame parent, JDialog child) {
        child.setLocationRelativeTo(parent);
        child.setVisible(true);
    }*//*

    public static File createFile(String path, String file) {
        try {

            String basePath = new File(".").getCanonicalPath();
            String settingsPath = basePath + "/" + path + "/" + file;
            File dirSetting = new File(basePath + "/" + path);
            if (!dirSetting.exists()) {
                dirSetting.mkdirs();
            }
            return new File(settingsPath);
            
        } catch (IOException ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (Exception ex) {
            Logger.getLogger(Util.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }*/

/**
 * Metodo para escribir en un archivo TXT una estructura JSON
 *
 * @param json estructura JSON
 * @param pathFile dsubdirectorio
 * @param file nombre del archivo
 */
    public static void writeFileJSON(Object json, String pathFile, String file) {
        String settingsPathFile = "";
        try {
            String basePath = new File(".").getCanonicalPath();
            settingsPathFile = basePath + "/" + pathFile + "/" + file;
            File dirSetting = new File(basePath + "/" + pathFile);
            if (!dirSetting.exists()) {
                dirSetting.mkdirs();
            }
            FileWriter fileWriter = new FileWriter(settingsPathFile);
            fileWriter.write(new GsonBuilder().setPrettyPrinting().create().toJson(json));
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException ex) {
            System.out.println("Error al Escribir File " + settingsPathFile);
        }
    }

}
