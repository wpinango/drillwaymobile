/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.variable;

import com.plv.drillwaymobile.dwmodels.Unit;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JoseAntonio
 */
public class Variable implements Cloneable {

    private DataLocal dataLocal;
    private DataMaster dataMaster;

    @java.lang.Override
    public Object clone() {
        Variable obj = null;
        try {
            obj = (Variable) super.clone();
        } catch (CloneNotSupportedException ex) {
            Logger.getLogger(Variable.class.getName()).log(Level.SEVERE, null, ex);
        }
        obj.dataLocal = (DataLocal) obj.dataLocal.clone();
        obj.dataMaster = (DataMaster) obj.dataMaster.clone();

        return obj;
    }

    public Variable(String nemo) {
        dataLocal = new DataLocal(nemo);
        dataMaster = new DataMaster();
    }

    public Unit getBaseUnit() {
        return dataMaster.getBaseUnit();

    }

    public DataLocal getDataLocal() {
        return dataLocal;
    }

    public void setDataLocal(DataLocal dataLocal) {
        this.dataLocal = dataLocal;
    }

    public DataMaster getDataMaster() {
        return dataMaster;
    }

    public void setDataMaster(DataMaster dataMaster) {
        this.dataMaster = dataMaster;
    }

}
