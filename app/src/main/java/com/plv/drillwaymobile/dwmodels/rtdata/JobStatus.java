/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.rtdata;

import com.plv.drillwaymobile.dwmodels.Enum;

import java.util.LinkedHashMap;

/**
 *
 * @author JoseAntonio
 */
public class JobStatus {

    private Enum.BitStatus bitStatus;
    private LinkedHashMap<Enum.Source, Enum.SourceStatus> sourceStatuses; 



    
    public JobStatus() {
        sourceStatuses = new LinkedHashMap<>();
        bitStatus = Enum.BitStatus.OFFBT;
    }

    
    
    public Enum.BitStatus getBitStatus() {
        return bitStatus;
    }

    public void setBitStatus(Enum.BitStatus bitStatus) {
        this.bitStatus = bitStatus;
    }

    public LinkedHashMap<Enum.Source, Enum.SourceStatus> getSourceStatuses() {
        return sourceStatuses;
    }

    public void setSourceStatuses(LinkedHashMap<Enum.Source, Enum.SourceStatus> sourceStatuses) {
        this.sourceStatuses = sourceStatuses;
    }

    
    
    
}
