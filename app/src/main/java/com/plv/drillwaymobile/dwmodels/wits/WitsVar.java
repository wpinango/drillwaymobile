/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.wits;

import com.plv.drillwaymobile.dwmodels.Enum;

/**
 *
 * @author JoseAntonio
 */
public class WitsVar {
    private String tag;
    private Enum.MNemonic nemonic;
    private Double value;
    private boolean active;

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Enum.MNemonic getNemonic() {
        return nemonic;
    }

    public void setNemonic(Enum.MNemonic nemonic) {
        this.nemonic = nemonic;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
}
