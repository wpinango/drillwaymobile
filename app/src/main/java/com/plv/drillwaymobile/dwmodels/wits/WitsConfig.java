/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile.dwmodels.wits;

import com.google.gson.Gson;

import static com.plv.drillwaymobile.dwmodels.Constant.PATH_SETTING;
import static com.plv.drillwaymobile.dwmodels.Util.writeFileJSON;

import java.util.ArrayList;

/**
 *
 * @author JoseAntonio
 */
public class WitsConfig {
    
    private String name;
    private int port;
    private int type;
    private boolean enabled;
    private ArrayList<Record> records;

    public WitsConfig() {
        name = "name";
        records = new ArrayList<>();     
        for (int i = 0; i < 4; i++) {           
            records.add(new Record(String.valueOf(i+1),"depth"));
        }
        
    }
    public void save(){
       writeFileJSON(this, PATH_SETTING, "witsconfig.json");
    }

    
    
    
    public ArrayList<Record> getRecords() {
        return records;
    }
    public void setRecords(ArrayList<Record> records) {
        this.records = records;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }
    
}
