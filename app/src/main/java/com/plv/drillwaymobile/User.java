/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.plv.drillwaymobile;

import com.plv.drillwaymobile.dwmodels.Enum;
import com.plv.drillwaymobile.dwmodels.variable.DataLocal;

import static com.plv.drillwaymobile.dwmodels.Constant.PATH_USER;
import static com.plv.drillwaymobile.dwmodels.Util.writeFileJSON;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 *
 * @author JoseAntonio
 */
public class User {
    private LinkedHashMap<Enum.MNemonic, DataLocal> varDataLocal;
    //private ArrayList<ScreenConfig> screenConfig;
    //private ScreenConfig fullScreenConfig;
    private String Name;

    @Override
    public String toString() {
        return Name;
    }

    /*public ScreenConfig getFullScreenConfig() {
        return fullScreenConfig;
    }

    public void setFullScreenConfig(ScreenConfig fullScreenConfig) {
        this.fullScreenConfig = fullScreenConfig;
    }*/
    
    
    
   public void save() {
        //writeFileJSON(this, PATH_USER, mainConfig.getActualUser());
    }
    public LinkedHashMap<Enum.MNemonic, DataLocal> getVarDataLocal() {
        return varDataLocal;
    }

    public void setVarDataLocal(LinkedHashMap<Enum.MNemonic, DataLocal> varDataLocal) {
        this.varDataLocal = varDataLocal;
    }


    /*public ArrayList<ScreenConfig> getScreenConfig() {
        return screenConfig;
    }*/

    /*public void setScreenConfig(ArrayList<ScreenConfig> screenConfig) {
        this.screenConfig = screenConfig;
    }*/



    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public DataLocal getVarDataLocal(Enum.MNemonic nemonic) {
        return varDataLocal.get(nemonic);
    }

    /*public ScreenConfig getScreenConfig(int i) {
        return  screenConfig.get(i);
    }*/
    
}
